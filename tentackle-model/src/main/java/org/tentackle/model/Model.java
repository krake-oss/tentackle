/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.model;

import org.tentackle.common.ServiceFactory;

import java.io.File;
import java.net.URL;
import java.util.Collection;


interface ModelHolder {
  Model INSTANCE = ServiceFactory.createService(Model.class);
}


/**
 * Model singleton.
 *
 * @author harald
 */
public interface Model {

  /**
   * Default name of the index file holding the model names within a jar or classpath.<br>
   * Resides in {@code META-INF}.
   */
  String DEFAULT_INDEX_NAME = "MODEL-INDEX.LIST";


  /**
   * The singleton.
   *
   * @return the singleton
   */
  static Model getInstance() {
    return ModelHolder.INSTANCE;
  }


  /**
   * Gets the entity factory singleton.
   *
   * @return the entity model factory
   */
  EntityFactory getEntityFactory();

  /**
   * Sets the model defaults to apply when loading the model sources.
   *
   * @param modelDefaults the model defaults, null if none
   */
  void setModelDefaults(ModelDefaults modelDefaults);

  /**
   * Gets the model defaults.
   *
   * @return the model defaults, null if none
   */
  ModelDefaults getModelDefaults();

  /**
   * Sets the entity aliases for relations between entities.
   *
   * @param entityAliases the aliases, null if none
   */
  void setEntityAliases(EntityAliases entityAliases);

  /**
   * Gets the entity aliases.
   *
   * @return the aliases, null if none
   */
  EntityAliases getEntityAliases();

  /**
   * Sets the name of the model index list.<br>
   * The default is {@link #DEFAULT_INDEX_NAME}.
   *
   * @param indexName the name of the list containing the entity model resource names
   */
  void setIndexName(String indexName);

  /**
   * Gets the name of the model index list.
   *
   * @return the name of the list containing the entity model resource names
   */
  String getIndexName();

  /**
   * Loads the whole model from the model directory if not yet done.
   *
   * @param modelDir the name of the directory containing the model files
   * @param updateRelations true if update the relations, false if model is still incomplete
   * @return the entity infos of the loaded model files
   * @throws ModelException if the model is inconsistent
   * @see #updateRelations()
   */
  Collection<EntityInfo> loadFromDirectory(String modelDir, boolean updateRelations) throws ModelException;

  /**
   * Loads an entity from a given URL.<br>
   * Entities are cached, so they are loaded and parsed only once.
   *
   * @param url the URL of the model file
   * @param updateRelations true if update the relations, false if model is still incomplete
   * @return the entity info
   * @throws ModelException if the model is inconsistent
   * @see #updateRelations()
   */
  EntityInfo loadFromURL(URL url, boolean updateRelations) throws ModelException;

  /**
   * Loads the model from a jar file.<br>
   * Reads the model files listed in {@code META-INF/MODEL-INDEX.LIST}.
   *
   * @param file the jar file
   * @param updateRelations true if update the relations, false if model is still incomplete
   * @return the entity infos of the loaded model files
   * @throws ModelException if the model is inconsistent
   */
  Collection<EntityInfo> loadFromJar(File file, boolean updateRelations) throws ModelException;

  /**
   * Loads from resources.<br>
   * Locates all {@code META-INF/MODEL-INDEX.LIST} resources and loads the model files.
   *
   * @param updateRelations true if update the relations, false if model is still incomplete
   * @return the entity infos of the loaded model files
   * @throws ModelException if the model is inconsistent
   */
  Collection<EntityInfo> loadFromResources(boolean updateRelations) throws ModelException;

  /**
   * Updates the relations if not yet done during load.
   *
   * @throws ModelException if model is inconsistent
   * @see #loadFromURL(URL, boolean)
   * @see #loadFromDirectory(String, boolean)
   */
  void updateRelations() throws ModelException;

  /**
   * Clears the model.
   */
  void clearModel();

  /**
   * Sets whether schema names should be mapped to the leading part of table names.
   *
   * @param mapSchemas true if map
   */
  void setSchemaNameMapped(boolean mapSchemas);

  /**
   * Returns whether schema names are mapped.
   *
   * @return true if mapped
   */
  boolean isSchemaNameMapped();

  /**
   * Refreshes the model.<br>
   * Checks for changes and reloads if necessary.
   *
   * @throws ModelException if the model is inconsistent
   */
  void refreshModel() throws ModelException;

  /**
   * Gets all cached entities.
   *
   * @return all entities loaded so far
   * @throws ModelException if the model is inconsistent
   */
  Collection<Entity> getAllEntities() throws ModelException;

  /**
   * Gets the entity by the model file path.
   *
   * @param url the URL of the model file
   * @return the entity, null if no such entity
   * @throws ModelException if the model is inconsistent
   */
  Entity getByURL(URL url) throws ModelException;

  /**
   * Gets the entity by its table name.
   *
   * @param tableName the table name (optional leading schema will be ignored)
   * @return the entity, null if no such entity
   * @throws ModelException if the model is inconsistent
   */
  Entity getByTableName(String tableName) throws ModelException;

  /**
   * Gets the entity by its name.
   *
   * @param entityName the name
   * @return the entity, null if no such entity
   * @throws ModelException if the model is inconsistent
   */
  Entity getByEntityName(String entityName) throws ModelException;

  /**
   * Gets the entity by its class id.
   *
   * @param classId the class id
   * @return the entity, null if no such entity
   * @throws ModelException if the model is inconsistent
   */
  Entity getByClassId(int classId) throws ModelException;

  /**
   * Gets the model information for the given entity.
   *
   * @param entity the entity
   * @return the model info, never null
   * @throws ModelException if the model is inconsistent or no such entity
   */
  EntityInfo getEntityInfo(Entity entity) throws ModelException;

  /**
   * Gets all foreign keys.
   *
   * @return the foreign keys
   * @throws ModelException if model is inconsistent
   */
  Collection<ForeignKey> getForeignKeys() throws ModelException;

}
