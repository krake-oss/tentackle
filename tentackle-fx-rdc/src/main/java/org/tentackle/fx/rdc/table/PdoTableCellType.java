/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.tentackle.fx.rdc.table;

import javafx.geometry.Pos;

import org.tentackle.fx.table.FxTableCell;
import org.tentackle.fx.table.FxTreeTableCell;
import org.tentackle.fx.table.TableCellTypeService;
import org.tentackle.fx.table.type.AbstractTableCellType;
import org.tentackle.pdo.PersistentDomainObject;

/**
 * PDO cell type.<br>
 * Not covered by the default ObjectTableCellType because PersistentDomainObject is an interface.<br>
 * This cell type does <em>not</em> set the graphics icon. If you need that, create an application-specific cell type.
 *
 * @author harald
 */
@TableCellTypeService(PersistentDomainObject.class)
public class PdoTableCellType<T extends PersistentDomainObject<T>> extends AbstractTableCellType<T> {

  /**
   * Creates the cell type for PDOs.
   */
  public PdoTableCellType() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public void updateItem(FxTableCell<?, T> tableCell, T pdo) {
    tableCell.setText(pdo.toString());
    tableCell.setGraphic(null);
    updateAlignment(tableCell, Pos.BASELINE_LEFT);
  }

  @Override
  public void updateItem(FxTreeTableCell<?, T> treeTableCell, T pdo) {
    treeTableCell.setText(pdo.toString());
    treeTableCell.setGraphic(null);
    updateAlignment(treeTableCell, Pos.BASELINE_LEFT);
  }

}
