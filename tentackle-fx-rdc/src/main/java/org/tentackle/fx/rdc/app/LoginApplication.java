/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.tentackle.fx.rdc.app;

import org.tentackle.app.Application;
import org.tentackle.common.Constants;
import org.tentackle.common.Cryptor;
import org.tentackle.common.EncryptedProperties;
import org.tentackle.common.StringHelper;
import org.tentackle.fx.Fx;
import org.tentackle.fx.FxUtilities;
import org.tentackle.fx.rdc.RdcRuntimeException;
import org.tentackle.fx.rdc.login.Login;
import org.tentackle.log.Logger;
import org.tentackle.misc.CommandLine;
import org.tentackle.session.SessionInfo;

import javafx.application.ColorScheme;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.awt.Rectangle;
import java.awt.SplashScreen;
import java.util.Locale;

/**
 * The login FX application.
 * <p>
 * Displays the login view and spawns a login handler.
 *
 * @author harald
 */
public class LoginApplication extends FxApplication {

  private static final Logger LOGGER = Logger.get(LoginApplication.class);


  private Login controller;       // the login controller


  /**
   * Creates the login application.
   */
  public LoginApplication() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public void startApplication(Stage stage) {
    try {
      Boolean darkMode = ((DesktopApplication<?>) Application.getInstance()).getConfiguredDarkMode();
      if (darkMode == null) {
        darkMode = Platform.getPreferences().getColorScheme() == ColorScheme.DARK;
      }
      FxUtilities.getInstance().setDarkMode(darkMode);

      controller = loadLoginController();
      SessionInfo sessionInfo = createSessionInfo();
      controller.setSessionInfo(sessionInfo);
      Scene scene = Fx.createScene(controller.getView());
      stage.setScene(scene);
      stage.initStyle(StageStyle.TRANSPARENT);
      SplashScreen splashScreen = SplashScreen.getSplashScreen();
      if (splashScreen != null) {
        // move center of login window to center of splash screen
        Rectangle bounds = splashScreen.getBounds();
        int splashX = bounds.x + bounds.width / 2;
        int splashY = bounds.y + bounds.height / 2;
        // this does the trick to avoid flickering on the screen due to the window first displayed and then moved
        stage.widthProperty().addListener((observable, oldValue, newValue) -> stage.setX(splashX - newValue.doubleValue() / 2.0 ));
        stage.heightProperty().addListener((observable, oldValue, newValue) -> stage.setY(splashY - newValue.doubleValue() / 2.0));
      }
      stage.setOnShown(e -> {
        if (splashScreen != null) {
          Platform.runLater(splashScreen::close);
        }
        if (((DesktopApplication<?>) Application.getInstance()).isAutoLogin()) {
          controller.autoLogin();   // login w/o option to edit the user and/or password
        }
        else if (sessionInfo.getUserName() != null && sessionInfo.getPassword() != null) {
          controller.login();       // perform a login once, allow the user to edit if login fails
        }
      });
      Fx.show(stage);
      stage.setResizable(false);
      controller.requestInitialFocus();
    }
    catch (RuntimeException rx) {
      throw new RdcRuntimeException("cannot create session info", rx);
    }
  }


  @Override
  public void showApplicationStatus(String msg, double progress) {
    if (controller.getView().isVisible()) {
      controller.setMessage(msg);
      controller.setProgress(progress);
    }
    else {
      LOGGER.info(msg);
    }
  }


  /**
   * Creates the session info to be passed to the login controller.
   *
   * @return the session info
   */
  public SessionInfo createSessionInfo() {
    DesktopApplication<?> application = (DesktopApplication<?>) Application.getInstance();
    CommandLine commandLine = application.getCommandLine();
    String username = commandLine.getOptionValue(Constants.BACKEND_USER);
    char[] password = StringHelper.toCharArray(commandLine.getOptionValue(Constants.BACKEND_PASSWORD));
    String sessionPropsName = commandLine.getOptionValue(Constants.BACKEND_PROPS);

    SessionInfo sessionInfo = application.createSessionInfo(username, password, sessionPropsName);
    sessionInfo.setSessionName("FX");

    // provide user and password from backend properties, if given
    EncryptedProperties props = sessionInfo.getProperties();
    if (username == null) {   // if not overridden by command line
      sessionInfo.setUserName(props.getProperty(Constants.BACKEND_USER, sessionInfo.getUserName()));
    }
    if (password == null) {   // if not overridden by command line: get from properties, if set
      sessionInfo.setPassword(props.getPropertyAsChars(Constants.BACKEND_PASSWORD));
    }

    sessionInfo.applyProperties();

    if (sessionInfo.getUserName() == null) {
      updateSessionInfoFromEnvironment(sessionInfo);
    }

    if (sessionInfo.getUserName() == null) {
      sessionInfo.setUserName(System.getProperty("user.name"));
    }

    application.applyProperties(sessionInfo.getProperties());

    return sessionInfo;
  }


  /**
   * Updates the session with username and password from the process environment.
   *
   * @param sessionInfo the session info to update
   */
  protected void updateSessionInfoFromEnvironment(SessionInfo sessionInfo) {
    Cryptor cryptor = Cryptor.getInstance();
    if (cryptor != null) {
      // Get user/passwd from environment.
      // Must be encrypted to provide a minimum level of security.
      // Used by the auto-update feature when restarting the updated application
      // (see org.tentackle.fx.rdc.update.LoginFailedWithUpdateHandler.passSessionInfoViaEnvironment)
      String envPrefix = StringHelper.toAsciiLetterOrDigit(Application.getInstance().getName());
      try {
        String encUser = System.getenv((envPrefix + "_" + Constants.BACKEND_USER).toUpperCase(Locale.ROOT));
        if (encUser != null) {
          sessionInfo.setUserName(Cryptor.getInstance().decrypt64(encUser));
          String encPass = System.getenv((envPrefix + "_" + Constants.BACKEND_PASSWORD).toUpperCase(Locale.ROOT));
          if (encPass != null) {
            sessionInfo.setPassword(Cryptor.getInstance().decrypt64ToChars(encPass));
          }
        }
      }
      catch (RuntimeException rx) {
        LOGGER.warning("decryption of username/password from environment failed", rx);
      }
    }
  }


  /**
   * Loads the login controller.<br>
   * Applications may override this method to use specific FXML or CSS files.
   *
   * @return the login controller
   */
  protected Login loadLoginController() {
    return Fx.load(Login.class);
  }

}
