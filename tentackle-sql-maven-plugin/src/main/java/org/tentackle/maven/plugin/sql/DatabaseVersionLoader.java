/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.maven.plugin.sql;

import org.apache.maven.plugin.MojoExecutionException;

import org.tentackle.sql.BackendInfo;

/**
 * Determines the database version in an application-specific way.<br>
 * The implementing class must provide a no-args constructor.
 *
 * @see BackendInfoParameter#minVersion
 */
public interface DatabaseVersionLoader {

  /**
   * Determines the database version.
   *
   * @param mojo the sql mojo
   * @param backendInfo the backend info
   * @return the version string
   * @throws MojoExecutionException if something failed
   */
  String getDatabaseVersion(AbstractSqlMojo mojo, BackendInfo backendInfo) throws MojoExecutionException;

}
