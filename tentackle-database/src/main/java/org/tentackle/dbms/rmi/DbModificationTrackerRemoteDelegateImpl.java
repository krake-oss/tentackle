/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.dbms.rmi;

import org.tentackle.dbms.DbModificationTracker;
import org.tentackle.misc.IdSerialNameTuple;
import org.tentackle.misc.IdSerialTuple;
import org.tentackle.session.MasterSerial;
import org.tentackle.session.MasterSerialEvent;
import org.tentackle.session.MasterSerialListEvent;
import org.tentackle.session.ModificationTracker;

import java.rmi.RemoteException;
import java.util.List;

/**
 * Remote delegate implementation for {@link DbModificationTracker}.
 * @param <T> the modification tracker class
 * @author  harald
 */
public class DbModificationTrackerRemoteDelegateImpl<T extends DbModificationTracker>
             extends RemoteDelegateImpl<T>
             implements DbModificationTrackerRemoteDelegate {

  public DbModificationTrackerRemoteDelegateImpl(RemoteDbSessionImpl session, Class<T> clazz) {
    super(session, clazz);
  }

  /**
   * Creates the master serial object from a long serial.
   * <p>
   * If {@link MasterSerialEvent}s are registered for this remote session,
   * the next event will be polled from the queue, its serial set and that event returned.
   *
   * @param serial the long master serial
   * @return the master serial object
   */
  protected MasterSerial createMasterSerial(long serial) {
    MasterSerialEvent masterSerialEvent = getServerSession().pollMasterSerialEvent();
    if (masterSerialEvent != null) {
      masterSerialEvent.setSerial(serial);
      MasterSerialEvent nextEvent = getServerSession().pollMasterSerialEvent();
      if (nextEvent != null) {
        MasterSerialListEvent listEvent = new MasterSerialListEvent();
        listEvent.setSerial(serial);
        listEvent.add(masterSerialEvent);
        while (nextEvent != null) {
          nextEvent.setSerial(serial);
          listEvent.add(nextEvent);
          nextEvent = getServerSession().pollMasterSerialEvent();
        }
        // only one element is possible due to MasterSerialEvent.isOverridingOlderEvents(), see MasterSerialListEvent.add()
        masterSerialEvent = listEvent.size() == 1 ? listEvent.get(0) : listEvent;
      }
      return masterSerialEvent;
    }
    return new DefaultMasterSerial(serial);
  }

  @Override
  public MasterSerial selectMasterSerial() throws RemoteException {
    try {
      getServerSession().setModificationTrackerSession(true);
      return createMasterSerial(((DbModificationTracker) ModificationTracker.getInstance()).getMasterSerial());
    }
    catch (RuntimeException ex)  {
      throw createException(ex);
    }
  }

  @Override
  public IdSerialTuple selectIdSerialForName(String tableName) throws RemoteException {
    try {
      return ((DbModificationTracker) ModificationTracker.getInstance()).getIdSerialForName(tableName);
    }
    catch (RuntimeException ex)  {
      throw createException(ex);
    }
  }

  @Override
  public List<IdSerialTuple> selectAllIdSerials() throws RemoteException {
    try {
      return ((DbModificationTracker) ModificationTracker.getInstance()).getAllIdSerials();
    }
    catch (RuntimeException ex)  {
      throw createException(ex);
    }
  }

  @Override
  public List<IdSerialNameTuple> selectAllIdSerialNames() throws RemoteException {
    try {
      return ((DbModificationTracker) ModificationTracker.getInstance()).getAllIdSerialNames();
    }
    catch (RuntimeException ex)  {
      throw createException(ex);
    }
  }

}
