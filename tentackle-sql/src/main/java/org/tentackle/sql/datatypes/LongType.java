/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.sql.datatypes;

import org.tentackle.common.Service;
import org.tentackle.common.StringHelper;
import org.tentackle.sql.Backend;
import org.tentackle.sql.DataType;
import org.tentackle.sql.DataTypeFactory;
import org.tentackle.sql.SqlType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Optional;

/**
 * Datatype for {@link Long}.
 */
@Service(DataType.class)
public sealed class LongType extends AbstractNumberType<Long> permits LongPrimitiveType {

  /**
   * Creates the datatype for {@link Long}.
   */
  public LongType() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public String getJavaType() {
    return "Long";
  }

  @Override
  public Optional<DataType<?>> toPrimitive() {
    return Optional.of(DataTypeFactory.getInstance().get("long"));
  }

  @Override
  public SqlType getSqlType(Backend backend, int index) {
    if (index != 0) {
      throw new IndexOutOfBoundsException();
    }
    return SqlType.BIGINT;
  }

  @Override
  public Long valueOf(String str) {
    return Long.valueOf(StringHelper.parseString(str));
  }

  @Override
  public String valueOfLiteralToCode(String str, Integer index) {
    if (!str.isEmpty()) {
      if (str.charAt(0) == '"') {
        return super.valueOfLiteralToCode(str, index);
      }
      if (Character.isDigit(str.charAt(str.length() - 1))) {
        return str + "L";
      }
    }
    return str;
  }

  @Override
  public Object[] set(Backend backend, PreparedStatement statement, int pos, Long object, boolean mapNull, Integer size) throws SQLException {
    if (object == null) {
      statement.setNull(pos, Types.BIGINT);
    }
    else {
      statement.setLong(pos, object);
    }
    return new Object[] { object };
  }

  @Override
  public Long get(Backend backend, ResultSet resultSet, int[] pos, boolean mapNull, Integer size) throws SQLException {
    return resultSet.getLong(pos[0]);
  }

}
