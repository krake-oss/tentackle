@{include $currentDir/header.incl}@
@{comment
<strong>({\@code \@wurblet})</strong> Generate code to check whether a low-level persistent object is referenced.
<p>
usage:<br>
&#064;wurblet &lt;tag&gt; DbIsReferencing [--private] [--append=&lt;sqltext&gt;] [--classvar=&lt;classvariables&gt;] &lt;expression&gt;
<p>
arguments:
<ul>
<li><em>--private:</em> makes the method private (default is public).</li>
<li><em>--append=&lt;sqltext&gt;:</em> appends an sql-string.</li>
<li><em>--classvar=&lt;classvariables&gt;:</em> reference to classvariables, if pick the statement-IDs from there.</li>
<li><em>&lt;expression&gt;:</em> see {\@link WurbletArgumentParser}.</li>
</ul>
For more options, see {\@link DbModelWurblet}.
}@
@[
  private void wurbel() throws WurbelException, ModelException {

    String className = getClassName();
    String methodName = getMethodName();

    String  classVar    = null;
    String  scope       = "public";
    String  append      = null;

    for (String arg: getOptionArgs())  {
      if (arg.equals("private")) {
        scope = "private";
        setRemote(false);
      }
      else if (arg.startsWith("append=")) {
        append = arg.substring(7);
      }
      else if (arg.startsWith("classvar="))  {
        classVar = arg.substring(9);
      }
    }

    if (classVar == null) {
      classVar = "";
    }
    else  {
      classVar += ".";    // add dot to access class var
    }

    if (getExpressionArguments().isEmpty())  {
      throw new WurbelException("no select keys given");
    }

    String params = buildMethodParameters();
    String iparms = buildInvocationParameters();
    String statementId = createStatementId();

]@
  @(scope)@ boolean @(methodName)@(@(params)@) {
@[

    if (isRemote()) {
      // create includes
      RemoteIncludes genInc = new RemoteIncludes(this);
      PrintStream implOut   = genInc.getImplementationStream();
      PrintStream remoteOut = genInc.getInterfaceStream();
]@
    if (getSession().isRemote())  {
      try {
@{to remoteOut}@
  boolean @(methodName)@(@(params)@) throws RemoteException;
@{to implOut}@

  \@Override
  public boolean @(methodName)@(@(params)@) throws RemoteException {
    try {
      return dbObject.@(methodName)@(@(iparms)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@
        return getRemoteDelegate().@(methodName)@(@(iparms)@);
      }
      catch (RemoteException e) {
        throw PersistenceException.createFromRemoteException(getSession(), e);
      }
    }
@[
    } // end if remote

]@
    PreparedStatementWrapper st = getPreparedStatement(@(classVar)@@(statementId)@,
      b -> {
        StringBuilder sql = createSelectIdInnerSql();
@{include $currentDir/genwhere.incl}@
        b.buildSelectSql(sql, false, 1, 0);
@[
    if (append != null) {
]@
        sql.append(' ').append(@(append)@);
@[
    }
]@
        return sql.toString();
      }
    );
    int ndx = getBackend().setLeadingSelectParameters(st, 1, 0);
@{include $currentDir/gensetpar.incl}@
    getBackend().setTrailingSelectParameters(st, ndx, 1, 0);
    try (ResultSetWrapper rs = st.executeQuery()) {
      return rs.next();
    }
  }
@[
    if (classVar.isEmpty()) {
]@

  private static final StatementId @(statementId)@ = new StatementId();

@[
    }
  }
]@
