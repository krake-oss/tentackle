@{include $currentDir/header.incl}@
@{comment
<strong>({\@code \@wurblet})</strong> Generate code for setting up a PdoCache for an entity.
<p>
usage:<br>
&#064;wurblet &lt;tag&gt; PdoCache
    [--secure] [--preload] [--mutable] [--udk]
    [--strategy=LRU|LFU|FORGET] [--maxsize=&lt;n&gt;] [--keepquota=&lt;p&gt;] [--configure=&lt;method&gt;]
    [index1] [index2] ...
<p>
arguments:
<ul>
<li><em>--secure:</em> if cache must check read permission for each access (default is no check).</li>
<li><em>--preload:</em> the first access will load all entities at once.</li>
<li><em>--mutable:</em> generates a non-shared cache, i.e. mutable PDOs (by default a shared readonly cache is generated).</li>
<li><em>--udk:</em> add index for the unique domain key.</li>
<li><em>--strategy=LRU|LFU|FORGET:</em> the caching strategy (default is FORGET).</li>
<li><em>--maxsize=&lt;n&gt;:</em> the cache size (if not preloaded).</li>
<li><em>--keepquota=&lt;p&gt;:</em> percentage of entries to keep when applying caching strategy. Default is 50.</li>
<li><em>--configure=&lt;method&gt;:</em> an optional method to further configure the cache.</li>
<li><em>[index&lt;n&gt;]:</em> additional unique indexes. By default, only the ID-index will be created.</li>
</ul>
Additional indexes are usually defined by their attribute name. Its values must be unique among all entities of the PDO class.
However, if the attribute name is followed by a method-name (w/o leading "get") in square brackets, that method is used to extract the key from the pdo
instead of the attribute's getter. This can be used to create a filtered index that contains only a subset of all PDOs,
if the method returns {\@code null} for PDOs that should not be part of the index.
<p>
Example:
<pre>
  // &#064;wurblet cache PdoCache --preload --udk objectId code[codeIfUser]
</pre>
<p>
For more options, see {\@link DbModelWurblet}.
}@
@[
  private void wurbel() throws ModelException, WurbelException {

    // main class
    String pdoName     = getPdoClassName();
    boolean isAbstract = getEntity().isAbstract();
    String pdoType     = isAbstract ? "T" : pdoName;

    boolean secure     = false;
    boolean preload    = false;
    boolean mutable    = false;
    boolean delayed    = true;
    boolean genudk     = false;

    String strategy    = null;
    Integer maxSize    = null;
    Integer keepQuota  = null;
    String configure   = null;

    for (String arg: getOptionArgs())  {
      if (arg.equals("preload")) {
        preload = true;
      }
      else if (arg.equals("secure")) {
        secure = true;
      }
      else if (arg.equals("mutable")) {
        mutable = true;
      }
      else if (arg.equals("immediate")) {
        delayed = false;
      }
      else if (arg.equals("udk")) {
        genudk = true;
      }
      else if (arg.startsWith("strategy=")) {
        strategy = arg.substring(9).toUpperCase(Locale.ROOT);
      }
      else if (arg.startsWith("maxsize=")) {
        maxSize = Integer.valueOf(arg.substring(8));
      }
      else if (arg.startsWith("keepquota=")) {
        keepQuota = Integer.valueOf(arg.substring(10));
      }
      else if (arg.startsWith("configure=")) {
        configure = arg.substring(10);
      }
    }

    if (secure && !getEntity().isRootEntity()) {
      throw new WurbelException(getEntity() + " is not a root-entity");
    }
    if (getEntity().getInheritanceType() == InheritanceType.PLAIN) {
      throw new WurbelException("only concrete (leaf) entity types can be cached within a PLAIN inheritance hierarchy");
    }

    List<Attribute> udk = null;
    String udkType = null;
    String udkName = null;
    String udkSuffix = null;
    if (genudk) {
      udk = getEntity().getUniqueDomainKey();
      if (!udk.isEmpty()) {
        if (udk.size() == 1) {
          udkType = getNonPrimitiveJavaType(udk.get(0));
          udkName = udk.get(0).getName();
          udkSuffix = udk.get(0).getMethodNameSuffix();
        }
        else {
          udkType = getEntity().getName() + "UDK";
          udkName = "udk";
          udkSuffix = "UniqueDomainKey";
          udkType = getEntity().getName() + "." + udkType;
        }
      }
      else {
        udk = null;
        genudk = false;
      }
    }

    StringBuilder listenNames = new StringBuilder();
    if (getEntity().getInheritanceType() == InheritanceType.MULTI) {
      for (Entity leaf : getEntity().getLeafEntities()) {
        if (!listenNames.isEmpty()) {
          listenNames.append(", ");
        }
        listenNames.append(leaf.getName()).append(".class");
      }
    }
    else {
      listenNames.append(pdoName).append(".class");
    }

]@
  /** Holder of the PDO cache singleton. */
  private static class CacheHolder {
@[
    if (isAbstract) {
]@
    private static final PdoCache<? extends @(pdoName)@<?>> CACHE = createCache();
@[
      for (WurbletArgument key: getExpressionArguments()) {
        String name = key.getMethodArgumentName();
        String type = getNonPrimitiveJavaType(key.getAttribute());
]@
    private static final PdoCacheIndex<? extends @(pdoName)@<?>, @(type)@> @(name.toUpperCase(Locale.ROOT) + "_INDEX")@ = create@(StringHelper.firstToUpper(name))@Index();
@[
      }

      if (genudk) {
]@
    private static final PdoCacheIndex<? extends @(pdoName)@<?>, @(udkType)@> UDK_INDEX = createUdkIndex();
@[
      }
]@

    \@SuppressWarnings("unchecked")
    private static <X extends @(pdoName)@<X>> PdoCache<X> createCache() {
      PdoCache<X> cache = (PdoCache<X>) Pdo.createCache(@(pdoName)@.class, @(preload ? "true" : "false")@, @(mutable ? "false" : "true")@, @(secure ? "true" : "false")@);
      Pdo.listen(cache::expire, @(listenNames)@);
@[
      if (strategy != null) {
]@
      cache.setStrategy(PdoCacheStrategy.@(strategy)@);
@[
      }
      if (maxSize != null) {
]@
      cache.setMaxSize(@(maxSize)@);
@[
      }
      if (keepQuota != null) {
]@
      cache.setKeepQuota(@(keepQuota)@);
@[
      }
      if (configure != null) {
]@
      @(configure)@(cache);
@[
      }
]@
      return cache;
    }
@[
      for (WurbletArgument key: getExpressionArguments()) {
        String name = key.getMethodArgumentName();
        String type = getNonPrimitiveJavaType(key.getAttribute());
        String suffix = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        if (isRemote()) {
          RemoteIncludes genInc = new RemoteIncludes(this);
          PrintStream implOut   = genInc.getImplementationStream();
          PrintStream remoteOut = genInc.getInterfaceStream();
]@
@{to remoteOut}@
  T selectBy@(suffix)@ForCache(DomainContext context, @(type)@ @(name)@) throws RemoteException;
@{to implOut}@

  \@Override
  public T selectBy@(suffix)@ForCache(DomainContext context, @(type)@ @(name)@) throws RemoteException {
    try {
      return newInstance(context).selectCachedBy@(suffix)@(@(name)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@

    \@SuppressWarnings("unchecked")
    private static <X extends @(pdoName)@<X>> PdoCacheIndex<X, @(type)@> create@(StringHelper.firstToUpper(name))@Index() {
      return Pdo.createCacheIndex("@(pdoName)@:@(name)@",
                                  (context, @(name)@) -> (X) Pdo.create(@(pdoName)@.class, context).selectBy@(suffix)@ForCache(@(name)@),
                                  @(pdoName)@::get@(suffix)@);
    }
@[
        } // end isRemote
        else {
]@

    \@SuppressWarnings("unchecked")
    private static <X extends @(pdoName)@<X>> PdoCacheIndex<X, @(type)@> create@(StringHelper.firstToUpper(name))@Index() {
      return Pdo.createCacheIndex("@(pdoName)@:@(name)@",
                                  (context, @(name)@) -> (X) Pdo.create(@(pdoName)@.class, context).findBy@(suffix)@(@(name)@),
                                  @(pdoName)@::get@(suffix)@);
    }
@[
        }
      }

      if (genudk) {
]@

    \@SuppressWarnings("unchecked")
    private static <X extends @(pdoName)@<X>> PdoCacheIndex<X, @(udkType)@> createUdkIndex() {
      return Pdo.createCacheIndex("@(pdoName)@:UDK",
@[
        if (isRemote()) {
          RemoteIncludes genInc = new RemoteIncludes(this);
          PrintStream implOut   = genInc.getImplementationStream();
          PrintStream remoteOut = genInc.getInterfaceStream();
]@
@{to remoteOut}@
  T selectByUniqueDomainKeyForCache(DomainContext context, @(udkType)@ @(udkName)@) throws RemoteException;
@{to implOut}@

  \@Override
  public T selectByUniqueDomainKeyForCache(DomainContext context, @(udkType)@ @(udkName)@) throws RemoteException {
    try {
      return newInstance(context).selectCachedByUniqueDomainKey(@(udkName)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@
                                  (context, @(udkName)@) -> (X) Pdo.create(@(pdoName)@.class, context).selectByUniqueDomainKeyForCache(@(udkName)@),
@[
        }
        else {
]@
                                  (context, @(udkName)@) -> (X) Pdo.create(@(pdoName)@.class, context).selectByUniqueDomainKey(@(udkName)@),
@[
        }
]@
                                  @(pdoName)@::get@(udkSuffix)@);
    }
@[
      }
]@
  }
@[

    }   // end isAbstract() ----------------------------------------------------------------
    else {

]@
    private static final PdoCache<@(pdoName)@> CACHE = createCache();
@[
      for (WurbletArgument key: getExpressionArguments()) {
        String name = key.getMethodArgumentName();
        String type = getNonPrimitiveJavaType(key.getAttribute());
]@
    private static final PdoCacheIndex<@(pdoName)@, @(type)@> @(name.toUpperCase(Locale.ROOT) + "_INDEX")@ = create@(StringHelper.firstToUpper(name))@Index();
@[
      }

      if (genudk) {
]@
    private static final PdoCacheIndex<@(pdoName)@, @(udkType)@> UDK_INDEX = createUdkIndex();
@[
      }
]@

    private static PdoCache<@(pdoName)@> createCache() {
      PdoCache<@(pdoName)@> cache = Pdo.createCache(@(pdoName)@.class, @(preload ? "true" : "false")@, @(mutable ? "false" : "true")@, @(secure ? "true" : "false")@);
      Pdo.listen(cache::expire, @(listenNames)@);
@[
      if (strategy != null) {
]@
      cache.setStrategy(PdoCacheStrategy.@(strategy)@);
@[
      }
      if (maxSize != null) {
]@
      cache.setMaxSize(@(maxSize)@);
@[
      }
      if (keepQuota != null) {
]@
      cache.setKeepQuota(@(keepQuota)@);
@[
      }
      if (configure != null) {
]@
      @(configure)@(cache);
@[
      }
]@
      return cache;
    }
@[
      for (WurbletArgument key: getExpressionArguments()) {
        String name = key.getMethodArgumentName();
        String type = getNonPrimitiveJavaType(key.getAttribute());
        String suffix = Character.toUpperCase(name.charAt(0)) + name.substring(1);
        if (isRemote()) {
          RemoteIncludes genInc = new RemoteIncludes(this);
          PrintStream implOut   = genInc.getImplementationStream();
          PrintStream remoteOut = genInc.getInterfaceStream();
]@
@{to remoteOut}@
  @(pdoType)@ selectBy@(suffix)@ForCache(DomainContext context, @(type)@ @(name)@) throws RemoteException;
@{to implOut}@

  \@Override
  public @(pdoType)@ selectBy@(suffix)@ForCache(DomainContext context, @(type)@ @(name)@) throws RemoteException {
    try {
      return newInstance(context).selectCachedBy@(suffix)@(@(name)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@

    private static PdoCacheIndex<@(pdoName)@, @(type)@> create@(StringHelper.firstToUpper(name))@Index() {
      return Pdo.createCacheIndex("@(pdoName)@:@(name)@",
                                  (context, @(name)@) -> Pdo.create(@(pdoName)@.class, context).selectBy@(suffix)@ForCache(@(name)@),
                                  @(pdoName)@::get@(suffix)@);
    }
@[
        } // end isRemote
        else {
]@

    private static PdoCacheIndex<@(pdoName)@, @(type)@> create@(StringHelper.firstToUpper(name))@Index() {
      return Pdo.createCacheIndex("@(pdoName)@:@(name)@",
                                  (context, @(name)@) -> Pdo.create(@(pdoName)@.class, context).findBy@(suffix)@(@(name)@),
                                  @(pdoName)@::get@(suffix)@);
    }
@[
        }
      }

      if (genudk) {
]@

    private static PdoCacheIndex<@(pdoName)@, @(udkType)@> createUdkIndex() {
      return Pdo.createCacheIndex("@(pdoName)@:UDK",
@[
        if (isRemote()) {
          RemoteIncludes genInc = new RemoteIncludes(this);
          PrintStream implOut   = genInc.getImplementationStream();
          PrintStream remoteOut = genInc.getInterfaceStream();
]@
@{to remoteOut}@
  @(pdoType)@ selectByUniqueDomainKeyForCache(DomainContext context, @(udkType)@ @(udkName)@) throws RemoteException;
@{to implOut}@

  \@Override
  public @(pdoType)@ selectByUniqueDomainKeyForCache(DomainContext context, @(udkType)@ @(udkName)@) throws RemoteException {
    try {
      return newInstance(context).selectCachedByUniqueDomainKey(@(udkName)@);
    }
    catch (RuntimeException e) {
      throw createException(e);
    }
  }
@{to}@
                                  (context, @(udkName)@) -> Pdo.create(@(pdoName)@.class, context).selectByUniqueDomainKeyForCache(@(udkName)@),
@[
        }
        else {
]@
                                  (context, @(udkName)@) -> Pdo.create(@(pdoName)@.class, context).selectByUniqueDomainKey(@(udkName)@),
@[
        }
]@
                                  @(pdoName)@::get@(udkSuffix)@);
    }
@[
      }
]@
  }
@[
    }

    String filterBegin = "";
    String filterEnd = "";

    if (getEntity().isAbstract()) {
      filterBegin = "filterByClassId(";
      filterEnd = ")";

      if (getEntity().getSuperEntity() == null) {
]@

  private boolean isValidClassId(T pdo) {
    if (pdo != null) {
      List<Integer> classIds = getValidClassIds();
      return classIds == null || classIds.contains(pdo.getPersistenceDelegate().getClassId());
    }
    return false;
  }

  private T filterByClassId(T pdo) {
    return isValidClassId(pdo) ? pdo : null;
  }

  private List<T> filterByClassId(List<T> pdos) {
    return getValidClassIds() == null ? pdos : pdos.stream().filter(this::isValidClassId).toList();
  }
@[
      }
      else {
]@

  private boolean isValidClassId(T pdo) {
    return pdo != null && getValidClassIds().contains(pdo.getPersistenceDelegate().getClassId());
  }

  private T filterByClassId(T pdo) {
    return isValidClassId(pdo) ? pdo : null;
  }

  private List<T> filterByClassId(List<T> pdos) {
    return pdos.stream().filter(this::isValidClassId).toList();
  }
@[
      }
    }

]@

  \@Override
@[
    if (isAbstract) {
]@
  \@SuppressWarnings("unchecked")
@[
    }
]@
  public PdoCache<@(pdoType)@> getCache() {
@[
    if (isAbstract) {
]@
    return (PdoCache<@(pdoType)@>) CacheHolder.CACHE;
@[
    }
    else {
]@
    return CacheHolder.CACHE;
@[
    }
]@
  }

  \@Override
  public boolean isCountingModification(ModificationType modType) {
    return true;
  }

  \@Override
  public boolean isReadAllowed() {
    return true;
  }

  \@Override
  public void expireCache(long maxSerial) {
    super.expireCache(maxSerial);
    CacheHolder.CACHE.expire(null, getTableName(), maxSerial);
  }

  \@Override
  public @(pdoType)@ selectCachedOnly(long id) {
    return @(filterBegin)@getCache().select(getDomainContext(), id, false)@(filterEnd)@;
  }

  \@Override
  public @(pdoType)@ selectCached(long id) {
    return @(filterBegin)@getCache().select(getDomainContext(), id)@(filterEnd)@;
  }
@[
    for (WurbletArgument key: getExpressionArguments()) {
      String name = key.getMethodArgumentName();
      String type = getNonPrimitiveJavaType(key.getAttribute());
      String suffix = Character.toUpperCase(name.charAt(0)) + name.substring(1);
      String indexCast = getEntity().isAbstract() ? ("(PdoCacheIndex<T, " + type + ">) ") : "";
      String indexName = "CacheHolder." + name.toUpperCase(Locale.ROOT) + "_INDEX";
      String indexMethod = "getCacheIndex" + StringHelper.firstToUpper(name);
]@

  /**
   * Gets the index for @(name)@.
   *
   * \@return the index for @(type)@ @(name)@
   */
@[
      if (isAbstract) {
]@
  \@SuppressWarnings("unchecked")
@[
      }
]@
  protected PdoCacheIndex<@(pdoType)@, @(type)@> @(indexMethod)@() {
    return @(indexCast)@@(indexName)@;
  }

  /**
   * Selects from cache by @(name)@ but does not load from db if not in cache.
   *
   * \@param @(name)@ the unique key
   * \@return the pdo, null if not in cache
   */
  \@Override
  public @(pdoType)@ selectCachedOnlyBy@(suffix)@(@(type)@ @(name)@)  {
    return @(filterBegin)@getCache().select(@(indexMethod)@(), getDomainContext(), @(name)@, false)@(filterEnd)@;
  }

  /**
   * Selects via cache by @(name)@.
   *
   * \@param @(name)@ the unique key
   * \@return the pdo, null if no such object
   */
  \@Override
  public @(pdoType)@ selectCachedBy@(suffix)@(@(type)@ @(name)@)  {
    return @(filterBegin)@getCache().select(@(indexMethod)@(), getDomainContext(), @(name)@)@(filterEnd)@;
  }

  /**
   * Selects via remote cache by @(name)@, if session is remote.
   *
   * \@param @(name)@ the unique key
   * \@return the pdo, null if no such object
   */
  \@Override
  public @(pdoType)@ selectBy@(suffix)@ForCache(@(type)@ @(name)@) {
    @(pdoType)@ obj;
    if (getSession().isRemote()) {
      try {
        DomainContext context = getDomainContext();
        obj = getRemoteDelegate().selectBy@(suffix)@ForCache(context, @(name)@);
        configureRemoteObject(context, obj);
      }
      catch (RemoteException e) {
        throw PersistenceException.createFromRemoteException(this, e);
      }
    }
    else {
      obj = me().findBy@(suffix)@(@(name)@);
    }
    return @(filterBegin)@obj@(filterEnd)@;
  }
@[
    }

    if (genudk) {
      String indexCast = isAbstract ? ("(PdoCacheIndex<T, " + udkType + ">) ") : "";
      String indexMethod = "getCacheIndexUdk";
]@

  /**
   * Gets the index for the unique domain key.
   *
   * \@return the index for @(udkType)@ @(udkName)@
   */
@[
      if (isAbstract) {
]@
  \@SuppressWarnings("unchecked")
@[
      }
]@
  protected PdoCacheIndex<@(pdoType)@, @(udkType)@> @(indexMethod)@() {
    return @(indexCast)@CacheHolder.UDK_INDEX;
  }

  /**
   * Selects from cache by unique domain key but does not load from db if not in cache.
   *
   * \@param @(udkName)@ the unique domain key
   * \@return the pdo, null if not in cache
   */
  \@Override
  public @(pdoType)@ selectCachedOnlyByUniqueDomainKey(@(udkType)@ @(udkName)@)  {
    return @(filterBegin)@getCache().select(@(indexMethod)@(), getDomainContext(), @(udkName)@, false)@(filterEnd)@;
  }

  /**
   * Selects via cache by unique domain key.
   *
   * \@param @(udkName)@ the unique key
   * \@return the pdo, null if no such object
   */
  \@Override
  public @(pdoType)@ selectCachedByUniqueDomainKey(@(udkType)@ @(udkName)@)  {
    return @(filterBegin)@getCache().select(@(indexMethod)@(), getDomainContext(), @(udkName)@)@(filterEnd)@;
  }

  /**
   * Selects via remote cache, if session is remote.
   *
   * \@param @(udkName)@ the unique key
   * \@return the pdo, null if no such object
   */
  \@Override
  public @(pdoType)@ selectByUniqueDomainKeyForCache(@(udkType)@ @(udkName)@) {
    @(pdoType)@ obj;
    if (getSession().isRemote()) {
      try {
        DomainContext context = getDomainContext();
        obj = getRemoteDelegate().selectByUniqueDomainKeyForCache(context, @(udkName)@);
        configureRemoteObject(context, obj);
      }
      catch (RemoteException e) {
        throw PersistenceException.createFromRemoteException(this, e);
      }
    }
    else {
      obj = me().findByUniqueDomainKey(@(udkName)@);
    }
    return @(filterBegin)@obj@(filterEnd)@;
  }
@[
    }

]@

  \@Override
  public List<@(pdoType)@> selectAllCached() {
    return @(filterBegin)@getCache().selectAll(getDomainContext())@(filterEnd)@;
  }
@[
  }
]@
