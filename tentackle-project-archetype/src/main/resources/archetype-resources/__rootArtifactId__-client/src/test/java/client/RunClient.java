#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.client;

import org.testng.annotations.Test;

public class RunClient {

  @Test
  public void runClient() {
    run(new String[] {"--statistics"});
  }

  @Test
  public void runClientDE() {
    run(new String[] {"--statistics", "--locale=de_DE"});
  }


  private synchronized void run(String[] args) {
    @SuppressWarnings("unchecked")
    ${application}FxClient client = new ${application}FxClient() {

      @Override
      protected boolean isSystemExitNecessaryToStop() {
        return false;   // no System.exit() by default within the test container
      }

      @Override
      public void stop(int exitValue, Throwable exitThrowable) {
        super.stop(exitValue, exitThrowable);
        if (exitValue != 0) {
          System.exit(exitValue);   // system exit only if error
        }
      }
    };
    client.start(args);
  }

}