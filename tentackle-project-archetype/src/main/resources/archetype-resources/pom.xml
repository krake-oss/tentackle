#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>\${groupId}</groupId>
  <artifactId>\${artifactId}</artifactId>
  <version>\${version}</version>
  <packaging>pom</packaging>

  <name>\${application} Parent</name>
  <description>Parent Project for \${application}</description>

  <modules>
    <module>\${rootArtifactId}-common</module>
    <module>\${rootArtifactId}-pdo</module>
    <module>\${rootArtifactId}-domain</module>
    <module>\${rootArtifactId}-persistence</module>
    <module>\${rootArtifactId}-gui</module>
    <module>\${rootArtifactId}-client</module>
    <module>\${rootArtifactId}-server</module>
    <module>\${rootArtifactId}-daemon</module>
    <!-- jlink images are built with profile "jlink" only, see jlink/pom.xml -->
    <module>jlink</module>
  </modules>

  <properties>
    <rootBasedir>\${symbol_dollar}{project.basedir}</rootBasedir>
    <maven.build.timestamp.format>yyyy-MM-dd HH:mm</maven.build.timestamp.format>

    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.build.resourceEncoding>UTF-8</project.build.resourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <wurbelizer.version>${wurbelizer.version}</wurbelizer.version>
    <wurbel.wurbelDir>\${symbol_dollar}{rootBasedir}/target/wurbel</wurbel.wurbelDir>

    <tentackle.version>${project.version}</tentackle.version>
    <tentackle.serviceDir>\${symbol_dollar}{project.build.directory}/generated-resources</tentackle.serviceDir>
    <tentackle.testServiceDir>\${symbol_dollar}{project.build.directory}/generated-test-resources</tentackle.testServiceDir>
    <tentackle.manifestDirectory>\${symbol_dollar}{project.build.directory}/generated-resources/manifest</tentackle.manifestDirectory>
    <tentackle.modelDir>\${symbol_dollar}{wurbel.wurbelDir}/model</tentackle.modelDir>
    <tentackle.sqlDir>\${symbol_dollar}{wurbel.wurbelDir}/sql</tentackle.sqlDir>
    <tentackle.modelDefaults>remote, bind, size, autoselect, tracked, root, rootid, rootclassid</tentackle.modelDefaults>

    <postgres.version>${postgresql.version}</postgres.version>
    <h2.version>${h2.version}</h2.version>
    <dbUrl>jdbc:postgresql://localhost/\${artifactId}</dbUrl>
    <dbUser>\${artifactId}</dbUser>
    <dbPasswd>\${artifactId}</dbPasswd>
    <dbService>rmi://localhost:8888/\${application}Server</dbService>
    <updateService>rmi://localhost:8890/\${application}Update</updateService>
    <updateURL>http://localhost/\${artifactId}/downloads</updateURL>

  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.tentackle</groupId>
        <artifactId>tentackle-bom</artifactId>
        <version>\${symbol_dollar}{tentackle.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>

      <!-- runtime dependencies -->
      <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <version>\${symbol_dollar}{postgres.version}</version>
        <scope>runtime</scope>
      </dependency>

      <!-- test dependencies -->
      <dependency>
        <groupId>org.testng</groupId>
        <artifactId>testng</artifactId>
        <version>${testng.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <!-- logger binder for testng -->
        <groupId>org.slf4j</groupId>
        <artifactId>slf4j-simple</artifactId>
        <version>${slf4j.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
        <version>\${symbol_dollar}{h2.version}</version>
        <scope>test</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <dependency>
      <groupId>org.testng</groupId>
      <artifactId>testng</artifactId>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-simple</artifactId>
    </dependency>
    <dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
    </dependency>
  </dependencies>

  <build>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.tentackle</groupId>
          <artifactId>tentackle-wizard-maven-plugin</artifactId>
          <version>\${symbol_dollar}{tentackle.version}</version>
          <configuration>
            <verbosity>info</verbosity>
            <url>\${symbol_dollar}{dbUrl}</url>
            <user>\${symbol_dollar}{dbUser}</user>
            <password>\${symbol_dollar}{dbPasswd}</password>
            <maxLinesInStringLiteral>10</maxLinesInStringLiteral>
            <profiles>
              <PdoProfile>
                <name>masterdata</name>
                <pdoPackage>\${package}.pdo.md</pdoPackage>
                <minClassId>1000</minClassId>
                <tablePrefix>md.</tablePrefix>
                <pdoInterface>MasterData</pdoInterface>
                <persistenceImplementation>AbstractPersistentMasterData</persistenceImplementation>
                <domainPackage>\${package}.pdo.md.domain</domainPackage>
                <persistencePackage>\${package}.pdo.md.persist</persistencePackage>
                <domainImplPackage>\${package}.domain.md</domainImplPackage>
                <persistenceImplPackage>\${package}.persist.md</persistenceImplPackage>
              </PdoProfile>
              <PdoProfile>
                <name>transactiondata</name>
                <pdoPackage>\${package}.pdo.td</pdoPackage>
                <minClassId>2000</minClassId>
                <tablePrefix>td.</tablePrefix>
                <pdoInterface>TransactionData</pdoInterface>
                <persistenceImplementation>AbstractPersistentTransactionData</persistenceImplementation>
                <domainPackage>\${package}.pdo.td.domain</domainPackage>
                <persistencePackage>\${package}.pdo.td.persist</persistencePackage>
                <domainImplPackage>\${package}.domain.td</domainImplPackage>
                <persistenceImplPackage>\${package}.persist.td</persistenceImplPackage>
              </PdoProfile>
              <OperationProfile>
                <name>operation</name>
                <operationPackage>\${package}.pdo.operation</operationPackage>
                <domainPackage>\${package}.pdo.operation</domainPackage>
                <persistencePackage>\${package}.pdo.operation</persistencePackage>
                <domainImplPackage>\${package}.domain.operation</domainImplPackage>
                <persistenceImplPackage>\${package}.persist.operation</persistenceImplPackage>
              </OperationProfile>
            </profiles>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>\${groupId}</groupId>
              <artifactId>${rootArtifactId}-gui</artifactId>
              <version>${symbol_dollar}{project.version}</version>
            </dependency>
            <dependency>
              <groupId>\${groupId}</groupId>
              <artifactId>${rootArtifactId}-persistence</artifactId>
              <version>${symbol_dollar}{project.version}</version>
            </dependency>
            <dependency>
              <groupId>\${groupId}</groupId>
              <artifactId>${rootArtifactId}-domain</artifactId>
              <version>${symbol_dollar}{project.version}</version>
            </dependency>
            <dependency>
              <groupId>org.tentackle</groupId>
              <artifactId>tentackle-test-pdo</artifactId>
              <version>\${symbol_dollar}{tentackle.version}</version>
            </dependency>
            <dependency>
              <groupId>org.postgresql</groupId>
              <artifactId>postgresql</artifactId>
              <version>\${symbol_dollar}{postgres.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.tentackle</groupId>
          <artifactId>tentackle-maven-plugin</artifactId>
          <version>\${symbol_dollar}{tentackle.version}</version>
          <executions>
            <execution>
              <id>analyze</id>
              <goals>
                <goal>analyze</goal>
              </goals>
            </execution>
            <execution>
              <id>test-analyze</id>
              <goals>
                <goal>test-analyze</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <verbosity>info</verbosity>
            <showCompileOutput>true</showCompileOutput>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.tentackle</groupId>
          <artifactId>tentackle-check-maven-plugin</artifactId>
          <version>\${symbol_dollar}{tentackle.version}</version>
          <executions>
            <execution>
              <id>bundles</id>
              <goals>
                <goal>bundles</goal>
              </goals>
            </execution>
            <execution>
              <id>validations</id>
              <goals>
                <goal>validations</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <verbosity>info</verbosity>
            <locales>en_US, de_DE</locales>
            <scriptingLanguage>groovy</scriptingLanguage>
            <showCompileOutput>true</showCompileOutput>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.tentackle</groupId>
              <artifactId>tentackle-script-groovy</artifactId>
              <version>\${symbol_dollar}{tentackle.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.tentackle</groupId>
          <artifactId>tentackle-i18n-maven-plugin</artifactId>
          <version>\${symbol_dollar}{tentackle.version}</version>
          <configuration>
            <verbosity>info</verbosity>
            <locales>de</locales>
            <url>\${symbol_dollar}{dbUrl}</url>
            <user>\${symbol_dollar}{dbUser}</user>
            <password>\${symbol_dollar}{dbPasswd}</password>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.postgresql</groupId>
              <artifactId>postgresql</artifactId>
              <version>\${symbol_dollar}{postgres.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.tentackle</groupId>
          <artifactId>tentackle-jlink-maven-plugin</artifactId>
          <version>\${symbol_dollar}{tentackle.version}</version>
          <extensions>true</extensions>
        </plugin>

        <plugin>
          <groupId>org.wurbelizer</groupId>
          <artifactId>wurbelizer-maven-plugin</artifactId>
          <version>\${symbol_dollar}{wurbelizer.version}</version>
          <executions>
            <execution>
              <id>wurbel</id>
              <goals>
                <goal>wurbel</goal>
              </goals>
            </execution>
            <execution>
              <id>test-wurbel</id>
              <goals>
                <goal>test-wurbel</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <wurbletDependencies>
              <wurbletDependency>
                <groupId>org.tentackle</groupId>
                <artifactId>tentackle-persistence-wurblets</artifactId>
                <version>\${symbol_dollar}{tentackle.version}</version>
              </wurbletDependency>
            </wurbletDependencies>
            <verbosity>info</verbosity>
            <filesets>
              <fileset>
                <includes>
                  <!-- PDO entity interfaces -->
                  <include>**/pdo/md/*.java</include>
                  <include>**/pdo/td/*.java</include>
                </includes>
              </fileset>
              <fileset>
                <includes>
                  <!-- PDO persistence interfaces -->
                  <include>**/pdo/md/persist/*.java</include>
                  <include>**/pdo/td/persist/*.java</include>
                  <!-- PDO domain interfaces -->
                  <include>**/pdo/md/domain/*.java</include>
                  <include>**/pdo/td/domain/*.java</include>
                </includes>
                <followSymlinks>false</followSymlinks>
              </fileset>
              <fileset>
                <includes>
                  <!-- domain implementations -->
                  <include>**/domain/md/*.java</include>
                  <include>**/domain/td/*.java</include>
                  <!-- persistence implementations -->
                  <include>**/persist/md/*.java</include>
                  <include>**/persist/td/*.java</include>
                  <!-- operation implementations -->
                  <include>**/persist/operation/*.java</include>
                </includes>
                <followSymlinks>false</followSymlinks>
              </fileset>
              <fileset>
                <includes>
                  <!-- remote delegates -->
                  <include>**/rmi/*.java</include>
                </includes>
                <followSymlinks>false</followSymlinks>
              </fileset>
            </filesets>
            <wurbletPaths>
              <param>org.tentackle.wurblet</param>
              <param>org.tentackle.persist.wurblet</param>
            </wurbletPaths>
            <wurbletProperties>
              <guardtype>netbeans</guardtype>
              <foldtype>collapsed</foldtype>
              <mapSchema>false</mapSchema>
              <tablePrefix />
              <model>\${symbol_dollar}{tentackle.modelDir}</model>
              <backends>postgresql, h2</backends>
              <integrity>full</integrity>
              <modelDefaults>\${symbol_dollar}{tentackle.modelDefaults}</modelDefaults>

              <!-- model column sizes -->
              <ou_name>20</ou_name>
              <ou_comment>256</ou_comment>
              <msg_type>30</msg_type>
              <msg_no>10</msg_no>
              <msg_ref>30</msg_ref>
              <msg_text>1024</msg_text>

            </wurbletProperties>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.tentackle</groupId>
          <artifactId>tentackle-sql-maven-plugin</artifactId>
          <version>\${symbol_dollar}{tentackle.version}</version>
          <configuration>
            <verbosity>info</verbosity>
            <modelDefaults>\${symbol_dollar}{tentackle.modelDefaults}</modelDefaults>
            <backends>
              <backend>
                <url>\${symbol_dollar}{dbUrl}</url>
                <user>\${symbol_dollar}{dbUser}</user>
                <password>\${symbol_dollar}{dbPasswd}</password>
                <schemaNames>td, md</schemaNames>
                <migrationHints>
                  <fileset>
                    <directory>\${symbol_dollar}{rootBasedir}/src/main/migrate/postgres</directory>
                    <includes>
                      <include>**/*.hints</include>
                    </includes>
                  </fileset>
                </migrationHints>
                <minVersion>select version from migration order by since desc limit 1</minVersion>
              </backend>
            </backends>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.postgresql</groupId>
              <artifactId>postgresql</artifactId>
              <version>\${symbol_dollar}{postgres.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven.compiler.plugin.version}</version>
          <configuration>
            <encoding>UTF-8</encoding>
            <release>${javac.version}</release>
            <showWarnings>true</showWarnings>
            <showDeprecation>true</showDeprecation>
            <compilerArgs>
              <arg>-Xlint:all,-processing,-exports,-missing-explicit-ctor,-this-escape</arg>
              <arg>-proc:full</arg>
            </compilerArgs>
            <!-- necessary since Java 9, see https://issues.apache.org/jira/browse/MCOMPILER-310 -->
            <annotationProcessorPaths>
              <dependency>
                <groupId>org.tentackle</groupId>
                <artifactId>tentackle-core</artifactId>
                <version>\${symbol_dollar}{tentackle.version}</version>
              </dependency>
              <dependency>
                <groupId>org.tentackle</groupId>
                <artifactId>tentackle-pdo</artifactId>
                <version>\${symbol_dollar}{tentackle.version}</version>
              </dependency>
              <dependency>
                <groupId>org.tentackle</groupId>
                <artifactId>tentackle-fx</artifactId>
                <version>\${symbol_dollar}{tentackle.version}</version>
              </dependency>
              <dependency>
                <groupId>org.tentackle</groupId>
                <artifactId>tentackle-fx-rdc</artifactId>
                <version>\${symbol_dollar}{tentackle.version}</version>
              </dependency>
              <dependency>    <!-- necessary for fx-rdc processors because deps are not recognized here -->
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-controls</artifactId>
                <version>${javafx.version}</version>
              </dependency>
            </annotationProcessorPaths>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${maven.resources.plugin.version}</version>
          <configuration>
            <encoding>UTF-8</encoding>
            <propertiesEncoding>UTF-8</propertiesEncoding>
            <escapeString>\</escapeString>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven.surefire.plugin.version}</version>
          <configuration>
            <argLine>-Duser.language=en -Duser.region=US</argLine>
          </configuration>
          <dependencies>
            <dependency>
              <groupId>org.ow2.asm</groupId>
              <artifactId>asm</artifactId>
              <version>${asm.version}</version> <!-- Use newer version of ASM -->
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-source-plugin</artifactId>
          <version>${maven.source.plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>${maven.javadoc.plugin.version}</version>
          <dependencies>
            <dependency>
              <groupId>org.ow2.asm</groupId>
              <artifactId>asm</artifactId>
              <version>${asm.version}</version>
            </dependency>
          </dependencies>
        </plugin>

        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>keytool-maven-plugin</artifactId>
          <version>1.7</version>
        </plugin>
      </plugins>
    </pluginManagement>

    <resources>
      <resource>
        <directory>\${symbol_dollar}{project.basedir}/src/main/resources</directory>
      </resource>
      <resource>
        <directory>\${symbol_dollar}{project.basedir}/src/main/filtered-resources</directory>
        <filtering>true</filtering>
      </resource>
      <!--  additional source directory for generated service files -->
      <resource>
        <directory>\${symbol_dollar}{tentackle.serviceDir}</directory>
      </resource>
    </resources>

    <testResources>
      <testResource>
        <directory>\${symbol_dollar}{project.basedir}/src/test/resources</directory>
      </testResource>
      <testResource>
        <directory>\${symbol_dollar}{project.basedir}/src/test/filtered-resources</directory>
        <filtering>true</filtering>
      </testResource>
      <testResource>
        <directory>\${symbol_dollar}{tentackle.testServiceDir}</directory>
      </testResource>
    </testResources>

    <plugins>
      <plugin>
        <groupId>org.tentackle</groupId>
        <artifactId>tentackle-maven-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.tentackle</groupId>
        <artifactId>tentackle-check-maven-plugin</artifactId>
      </plugin>
    </plugins>

  </build>

  <profiles>
    <profile>
      <!-- generate sources and javadoc -->
      <id>release</id>
      <build>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-source-plugin</artifactId>
            <executions>
              <execution>
                <id>attach-sources</id>
                <goals>
                  <goal>jar-no-fork</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <executions>
              <execution>
                <id>attach-javadocs</id>
                <goals>
                  <goal>jar</goal>
                </goals>
                <configuration>
                  <quiet>true</quiet>
                  <doclint>none</doclint>
                </configuration>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

</project>
