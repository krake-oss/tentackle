#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.persist.md.rmi;

import ${package}.pdo.md.User2Group;
import ${package}.persist.md.User2GroupPersistenceImpl;

import org.tentackle.misc.TrackedList;
import org.tentackle.pdo.DomainContext;

import java.rmi.RemoteException;

/**
 * Remote delegate for {@link User2GroupPersistenceImpl}.
 */
public interface User2GroupRemoteDelegate
       extends AbstractPersistentMasterDataRemoteDelegate<User2Group,User2GroupPersistenceImpl> {

  // @wurblet inclrmi Include --missingok .${symbol_dollar}classname/methods

}
