#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.persist.md.rmi;

import ${package}.pdo.md.User;
import ${package}.persist.md.UserPersistenceImpl;

import org.tentackle.dbms.rmi.RemoteDbSessionImpl;

import java.rmi.RemoteException;

/**
 * Remote delegate implementation for {@link UserPersistenceImpl}.
 */
public class UserRemoteDelegateImpl
       extends OrgUnitRemoteDelegateImpl<User,UserPersistenceImpl>
       implements UserRemoteDelegate {

  /**
   * Creates the remote delegate for {@link UserPersistenceImpl}.
   *
   * @param session the RMI session
   * @param persistenceClass the persistence implementation class
   * @param pdoClass the pdo interface class
   */
  public UserRemoteDelegateImpl(RemoteDbSessionImpl session, Class<UserPersistenceImpl> persistenceClass, Class<User> pdoClass) {
    super(session, persistenceClass, pdoClass);
  }

  // @wurblet inclrmi Include --missingok .${symbol_dollar}classname/methods

}
