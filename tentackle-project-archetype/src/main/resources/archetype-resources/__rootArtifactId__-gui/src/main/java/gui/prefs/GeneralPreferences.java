#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * ${application} generated by tentackle-project-archetype.
 */

package ${package}.gui.prefs;

import javafx.fxml.FXML;

import org.tentackle.fx.FxControllerService;
import org.tentackle.fx.component.FxTextField;

/**
 * Tab for general preferences.
 */
@FxControllerService(binding = FxControllerService.BINDING.BINDABLE_INHERITED)
public class GeneralPreferences extends AbstractPreferencesTabController {

  @FXML
  private FxTextField prefHelpUrl;

}
