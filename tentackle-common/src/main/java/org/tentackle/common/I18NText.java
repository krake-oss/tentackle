/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.common;

import java.io.Serial;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

/**
 * Multilingual text.<br>
 * This is an immutable and serializable object which is supported as a regular datatype
 * via <code>org.tentackle.sql.datatypes.I18NTextType</code>.
 * <p>
 * Model example:<br>
 * <pre>
 *   ...
 *   I18NText        description   description    internationalized description [COLS=30]
 *   ...
 * </pre>
 * <p>
 * Don't forget to add the <code>[COLS=...]</code> option if yoy want to use auto-binding for the field width in FX views.
 * <p>
 * Please keep in mind, that <code>I18NText</code> implements
 * {@link CharSequence} and {@link Comparable} only for the current locale (see {@code  LocaleProvider.getInstance().getLocale()}),
 * but {@link Object#equals(Object)} and {@link Object#hashCode()} cover all translations.
 * As a consequence, sorted sets of <code>I18NText</code> elements require appropriate attention.
 */
public class I18NText implements CharSequence, Comparable<I18NText>, Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  /** parameter name for the default locale. */
  private static final String DEFAULT_PAR = "default";


  /**
   * Creates an <code>I18NText</code> from a string in the current locale.
   *
   * @param text the string
   * @return the i18n object
   */
  public static I18NText valueOf(String text) {
    return new I18NText(text);
  }


  /**
   * Localized strings.<br>
   * {@link LinkedHashMap} to keep the order of insertions.
   */
  private final LinkedHashMap<Locale, String> textMap;

  /**
   * The default locale, if the value hasn't been set for the fallback locale so far.<br>
   * Null if textMap is empty or the translation for the fallback language is provided.
   */
  private final Locale defaultLocale;


  /**
   * Creates a vacant <code>I18NText</code>.
   */
  public I18NText() {
    textMap = new LinkedHashMap<>();
    defaultLocale = null;
  }

  /**
   * Creates an <code>I18NText</code> from a string in the current locale.
   *
   * @param text the string
   */
  public I18NText(String text) {
    this(LocaleProvider.getInstance().getLocale(), text);
  }

  /**
   * Creates an <code>I18NText</code> from a string for the given locale.
   *
   * @param locale the locale
   * @param text the string
   */
  public I18NText(Locale locale, String text) {
    textMap = new LinkedHashMap<>();
    defaultLocale = apply(locale, text, textMap, null);
  }

  /**
   * Creates an <code>I18NText</code> from a parameter string.
   *
   * @param parameterString the parameter string
   * @see #toParameterString()
   */
  public I18NText(ParameterString parameterString) {
    LocaleProvider localeProvider = LocaleProvider.getInstance();
    String parameter = parameterString.getParameter(DEFAULT_PAR);
    defaultLocale = parameter == null ? null : localeProvider.fromTag(parameter);
    textMap = new LinkedHashMap<>();
    for (String tag : parameterString.getParameterNames()) {
      Locale locale = localeProvider.fromTag(tag);
      if (locale != null && localeProvider.isLocaleSupported(locale)) {
        parameter = parameterString.getParameter(tag);
        if (parameter != null) {
          textMap.put(localeProvider.getEffectiveLocale(locale), parameter);
        }
      }
    }
  }


  /**
   * Derives an I18NText object with a translation for the given locale.
   *
   * @param locale the locale
   * @param text the string
   * @return the old translation, null if new
   */
  @SuppressWarnings("unchecked")
  public I18NText with(Locale locale, String text) {
    LinkedHashMap<Locale, String> withTextMap = (LinkedHashMap<Locale, String>) textMap.clone();
    Locale withDefaultLocale = apply(locale, text, withTextMap, defaultLocale);
    return new I18NText(withTextMap, withDefaultLocale);
  }

  /**
   * Derives an I18NText object with a translation for the current locale.
   *
   * @param text the string
   * @return the old translation, null if new
   */
  public I18NText with(String text) {
    return with(LocaleProvider.getInstance().getLocale(), text);
  }

  /**
   * Gets the translation for the given locale.
   *
   * @param locale the locale
   * @return the string, null if no translations at all
   */
  public String get(Locale locale) {
    LocaleProvider localeProvider = LocaleProvider.getInstance();
    String text = textMap.get(narrowLocale(localeProvider.getEffectiveLocale(locale)));
    if (text == null) {
      if (defaultLocale != null) {
        text = textMap.get(defaultLocale);
      }
      else {
        text = textMap.get(localeProvider.getFallbackLocale());
      }
    }
    return text;
  }

  /**
   * Gets the translation for the current locale.
   *
   * @return the string, null if no translations at all
   */
  public String get() {
    return get(LocaleProvider.getInstance().getLocale());
  }

  /**
   * Gets the current default locale.<br>
   *
   * @return the default locale, null if no translations at all or the translation for the fallback locale is set
   */
  public Locale getDefaultLocale() {
    return defaultLocale;
  }

  /**
   * Gets the mapping between locales and translations.
   *
   * @return the unmodifiable map of translations
   */
  public Map<Locale, String> getTranslations() {
    return Map.copyOf(textMap);
  }

  /**
   * Creates a parameter string of the current translations.
   *
   * @return the parameter string
   */
  public ParameterString toParameterString() {
    ParameterString ps = new ParameterString();
    LocaleProvider localeProvider = LocaleProvider.getInstance();
    if (defaultLocale != null) {
      ps.setParameter(DEFAULT_PAR, localeProvider.toTag(defaultLocale));
    }
    for (Map.Entry<Locale, String> entry : textMap.entrySet()) {
      ps.setParameter(localeProvider.toTag(entry.getKey()), entry.getValue());
    }
    return ps;
  }

  /**
   * Returns whether there are valid translations at all.
   *
   * @return true if the map of translations is empty
   */
  public boolean isVacant() {
    return textMap.isEmpty();
  }


  @Override
  public int length() {
    String text = get();
    return text == null ? 0 : text.length();
  }

  @Override
  public boolean isEmpty() {
    return isVacant() || length() == 0;
  }

  @Override
  public char charAt(int index) {
    String text = get();
    if (text != null) {
      return text.charAt(index);
    }
    throw new IndexOutOfBoundsException("no translation for the current locale");
  }

  @Override
  public CharSequence subSequence(int start, int end) {
    String text = get();
    return text == null ? "".subSequence(start, end) : text.subSequence(start, end);
  }

  @Override
  public IntStream chars() {
    String text = get();
    int length = text == null ? 0 : text.length();

    class CharIterator implements PrimitiveIterator.OfInt {
      int cur = 0;

      public boolean hasNext() {
        return cur < length;
      }

      public int nextInt() {
        if (hasNext()) {
          return text.charAt(cur++);
        }
        else {
          throw new NoSuchElementException();
        }
      }

      @Override
      public void forEachRemaining(IntConsumer block) {
        for (; cur < length; cur++) {
          block.accept(text.charAt(cur));
        }
      }
    }

    return StreamSupport.intStream(() ->
                                     Spliterators.spliterator(
                                       new CharIterator(),
                                       length,
                                       Spliterator.ORDERED),
      Spliterator.SUBSIZED | Spliterator.SIZED | Spliterator.ORDERED,
      false);
  }

  @Override
  public IntStream codePoints() {
    String text = get();
    int length = text == null ? 0 : text.length();

    class CodePointIterator implements PrimitiveIterator.OfInt {
      int cur = 0;

      @Override
      public void forEachRemaining(IntConsumer block) {
        int i = cur;
        try {
          while (i < length) {
            char c1 = text.charAt(i++);
            if (!Character.isHighSurrogate(c1) || i >= length) {
              block.accept(c1);
            }
            else {
              char c2 = text.charAt(i);
              if (Character.isLowSurrogate(c2)) {
                i++;
                block.accept(Character.toCodePoint(c1, c2));
              }
              else {
                block.accept(c1);
              }
            }
          }
        }
        finally {
          cur = i;
        }
      }

      public boolean hasNext() {
        return cur < length;
      }

      public int nextInt() {
        if (cur >= length) {
          throw new NoSuchElementException();
        }
        char c1 = text.charAt(cur++);
        if (Character.isHighSurrogate(c1) && cur < length) {
          char c2 = text.charAt(cur);
          if (Character.isLowSurrogate(c2)) {
            cur++;
            return Character.toCodePoint(c1, c2);
          }
        }
        return c1;
      }
    }

    return StreamSupport.intStream(() ->
                                     Spliterators.spliteratorUnknownSize(
                                       new CodePointIterator(),
                                       Spliterator.ORDERED),
      Spliterator.ORDERED,
      false);
  }

  @Override
  public int compareTo(I18NText o) {
    if (this == o) {
      return 0;
    }
    String text = get();
    String oText = o.get();
    if (text == null) {
      return oText == null ? 0 : -1;
    }
    return oText == null ? 1 : text.compareTo(oText);
  }

  @Override
  public String toString() {
    return get();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    I18NText i18NText = (I18NText) o;

    if (!textMap.equals(i18NText.textMap)) {
      return false;
    }
    return Objects.equals(defaultLocale, i18NText.defaultLocale);
  }

  @Override
  public int hashCode() {
    int result = textMap.hashCode();
    result = 31 * result + (defaultLocale != null ? defaultLocale.hashCode() : 0);
    return result;
  }


  /**
   * Protected constructor for internal use only.
   *
   * @param textMap the translations
   * @param defaultLocale the default locale
   */
  protected I18NText(LinkedHashMap<Locale, String> textMap, Locale defaultLocale) {
    this.textMap = textMap;
    this.defaultLocale = defaultLocale;
  }

  /**
   * Applies a translation to an existing map and default locale.
   *
   * @param locale the locale to apply
   * @param text the translation to apply
   * @param withTextMap the current translations
   * @param withDefaultLocale the current default locale
   * @return the new default locale
   */
  protected Locale apply(Locale locale, String text, LinkedHashMap<Locale, String> withTextMap, Locale withDefaultLocale) {
    LocaleProvider localeProvider = LocaleProvider.getInstance();
    if (!localeProvider.isLocaleSupported(locale)) {
      // avoid overriding the fallback locale by an unsupported locale (see getEffectiveLocale below)
      throw new TentackleRuntimeException("unsupported locale: " + locale);
    }

    locale = narrowLocale(localeProvider.getEffectiveLocale(locale));

    if (StringHelper.isAllWhitespace(text)) {
      // remove locale
      withTextMap.remove(locale);
      if (locale.equals(withDefaultLocale)) {   // default overrides fallback
        withDefaultLocale = null;
        if (!withTextMap.isEmpty()) {
          // take the default from the locale that was added next after the old default
          withDefaultLocale = localeProvider.getEffectiveLocale(withTextMap.keySet().iterator().next());
        }
        // else: now empty -> no default anymore
      }
    }
    else {
      // add locale
      if (withDefaultLocale == null) {
        if (withTextMap.isEmpty() && !locale.equals(localeProvider.getFallbackLocale())) {
          withDefaultLocale = localeProvider.getEffectiveLocale(locale);   // first locale, if not the fallback, determines the temporary default locale
        }
      }
      else if (locale.equals(localeProvider.getFallbackLocale())) {
        withDefaultLocale = null;   // fallback is now the default
      }
      withTextMap.put(locale, text);
    }

    return withDefaultLocale;
  }

  /**
   * Narrows the effective locale returned by the {@link LocaleProvider} further to language.<br>
   * The LocaleProvider may allow wider locales for ResourceBundles,
   * but for I18NText this would complicate things way too much.<br>
   * In most applications, the LocaleProvider narrows to language anyway.
   *
   * @param locale the effective locale
   * @return the language-only locale
   */
  protected Locale narrowLocale(Locale locale) {
    if (!locale.getVariant().isEmpty() || !locale.getCountry().isEmpty()) {
      // variant or country given: narrow to language only.
      // The LocaleProvider may allow wider locales for ResourceBundles,
      // but for I18NText this would complicate things way too much.
      // In most applications, the LocaleProvider narrows to language anyway.
      locale = Locale.of(locale.getLanguage());
    }
    return locale;
  }

}
