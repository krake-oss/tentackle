/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.tentackle.fx.container.build;

import org.tentackle.fx.AbstractBuilder;
import org.tentackle.fx.BuilderService;
import org.tentackle.fx.container.FxTab;

import javafx.beans.DefaultProperty;
import javafx.scene.control.Tab;

/**
 * Builder to create a Tab.
 *
 * @author harald
 */
@BuilderService(Tab.class)
@DefaultProperty("content")
public class TabBuilder extends AbstractBuilder<Tab> {

  /**
   * Creates the builder.
   */
  public TabBuilder() {
    super(new FxTab(), "content");
  }

}
