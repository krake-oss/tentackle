/*
 * Tentackle - https://tentackle.org.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package org.tentackle.fx.component.config;

import org.tentackle.fx.FxComponent;

import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Configures a {@link TextInputControl}.
 *
 * @author harald
 * @param <T> the text field type
 */
public abstract class TextInputControlConfigurator<T extends TextInputControl> extends ComponentConfigurator<T> {

  /**
   * Parent constructor.
   */
  public TextInputControlConfigurator() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public void configure(T control) {
    super.configure(control);

    // first keystroke triggers check for modification
    control.textProperty().addListener(o -> ((FxComponent) control).triggerViewModified());

    // Shift-Backspace clears the field
    control.addEventFilter(KeyEvent.ANY, (KeyEvent event) -> {

      if (event.getCode() == KeyCode.BACK_SPACE && event.isShiftDown() &&
          !event.isControlDown() && !event.isAltDown() && !event.isMetaDown()) {

        if (event.getEventType() == KeyEvent.KEY_PRESSED &&
            !control.isDisabled() && control.isEditable()) {
          control.clear();
        }
        event.consume();
      }
    });
  }


  @Override
  protected void remapKeys(T control) {

    control.addEventFilter(KeyEvent.ANY, (KeyEvent event) -> {

      if (event.getCode() == KeyCode.ENTER &&
          !event.isControlDown() && !event.isAltDown() && !event.isMetaDown() &&
          event.getEventType() == KeyEvent.KEY_PRESSED) {

        if (event.isShiftDown()) {
          focusPrevious(control);
        }
        else {
          focusNext(control);
        }
      }
    });
  }


  /**
   * Moves the focus logically forward if not a table cell editor.
   *
   * @param control the control
   */
  protected void focusNext(T control) {
    if (!(control instanceof FxComponent) || !((FxComponent) control).isCellEditor()) {
      //      NodeHelper.traverse(control, Direction.NEXT);   is non-public API :(
      KeyEvent event = new KeyEvent(KeyEvent.KEY_PRESSED, KeyEvent.CHAR_UNDEFINED, "", KeyCode.TAB,
                                    false, false, false, false);
      control.fireEvent(event);
    }
  }

  /**
   * Moves the focus logically back if not a table cell editor.
   *
   * @param control the control
   */
  protected void focusPrevious(T control) {
    if (!(control instanceof FxComponent) || !((FxComponent) control).isCellEditor()) {
      //      NodeHelper.traverse(control, Direction.PREVIOUS);   is non-public API :(
      KeyEvent event = new KeyEvent(KeyEvent.KEY_PRESSED, KeyEvent.CHAR_UNDEFINED, "", KeyCode.TAB,
                                    true, false, false, false);
      control.fireEvent(event);
    }
  }

}
