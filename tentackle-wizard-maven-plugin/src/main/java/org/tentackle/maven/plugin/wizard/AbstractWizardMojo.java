/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


package org.tentackle.maven.plugin.wizard;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;

import org.tentackle.fx.Fx;
import org.tentackle.fx.FxUtilities;
import org.tentackle.maven.AbstractTentackleMojo;
import org.tentackle.model.EntityAliases;
import org.tentackle.model.Model;
import org.tentackle.model.ModelDefaults;
import org.tentackle.model.ModelException;

import javafx.application.ColorScheme;
import javafx.application.Platform;
import javafx.stage.Stage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Base wizard mojo.
 *
 * @author harald
 */
public abstract class AbstractWizardMojo extends AbstractTentackleMojo {

  /**
   * Directory holding the model files to be processed.<br>
   * Ignored if fileset explicitly given.
   */
  @Parameter(defaultValue = "${project.build.directory}/wurbel/model", property = "tentackle.modelDir")
  protected File modelDir;

  /**
   * Directory holding the status info.<br>
   * Used to store the next free classId per profile in case multiple PDOs are
   * generated without an intermediate mvn clean package/install or wurbel run.<br>
   * Must be a subdir of the target directory to be removed via mvn clean.
   */
  @Parameter(defaultValue = "${project.build.directory}/wizard")
  private File statusDir;

  /**
   * The model defaults.
   */
  @Parameter(property = "tentackle.modelDefaults")
  protected String modelDefaults;

  /**
   * The entity aliases.
   */
  @Parameter(property = "tentackle.entityAliases")
  protected String entityAliases;

  /**
   * The list of file sets containing the model.<br>
   * If set, the modelDir is ignored.
   */
  @Parameter
  protected List<FileSet> filesets;

  /**
   * The profiles.
   */
  @Parameter(required = true)
  private List<Profile> profiles;

  /**
   * The directory holding the templates.
   */
  @Parameter(defaultValue = "${project.basedir}/templates")
  private File templateDir;

  /**
   * Load the model from the plugin dependencies on the classpath.
   */
  @Parameter(defaultValue = "true")
  private boolean loadModelFromDependencies;


  /**
   * Enables dark mode.
   */
  @Parameter
  private Boolean darkMode;


  /**
   * Gets the profiles.
   *
   * @param <T> the profile type
   * @param clazz the concrete profile class
   * @return the list of profiles
   * @throws MojoExecutionException if profile names are not unique
   */
  @SuppressWarnings("unchecked")
  public <T extends Profile> List<T> getProfiles(Class<T> clazz) throws MojoExecutionException {
    List<T> list = new ArrayList<>();
    for (Profile profile : profiles) {
      if (clazz.isAssignableFrom(profile.getClass())) {
        if (list.contains(profile)) {
          throw new MojoExecutionException("profile " + profile + " configured more than once");
        }
        list.add((T) profile);
      }
    }
    return list;
  }


  /**
   * Gets the template directory.
   *
   * @return the directory holding the wizard templates
   */
  public File getTemplateDir() {
    return templateDir;
  }

  /**
   * Gets the status directory.
   *
   * @return the status dir
   */
  public File getStatusDir() {
    return statusDir;
  }

  @Override
  public void executeImpl() throws MojoExecutionException, MojoFailureException {
    loadModel();
  }


  /**
   * Gets the model defaults.
   *
   * @return the defaults, null if none
   * @throws MojoExecutionException if parsing the model defaults failed
   */
  protected ModelDefaults getModelDefaults() throws MojoExecutionException {
    if (modelDefaults == null) {
      return null;
    }
    try {
      return new ModelDefaults(modelDefaults);
    }
    catch (ModelException mex) {
      throw new MojoExecutionException(mex.getMessage(), mex);
    }
  }


  /**
   * Gets the entity aliases.
   *
   * @return the aliases, null if none
   * @throws MojoExecutionException if parsing the aliases failed
   */
  protected EntityAliases getEntityAliases() throws MojoExecutionException {
    if (entityAliases == null) {
      return null;
    }
    try {
      return new EntityAliases(entityAliases);
    }
    catch (ModelException mex) {
      throw new MojoExecutionException(mex.getMessage(), mex);
    }
  }

  /**
   * Loads the model.
   * <p>
   * Notice: the model is only needed by the {@link PdoMojo}, but we configure it for the whole plugin.
   *
   * @throws MojoFailureException if the model is inconsistent
   * @throws MojoExecutionException if loading failed due to some other error
   */
  protected void loadModel() throws MojoFailureException, MojoExecutionException {
    int errors = 0;

    if (loadModelFromDependencies) {
      try {
        Model.getInstance().loadFromResources(true);
      }
      catch (ModelException e) {
        throw new MojoFailureException(e);
      }
    }

    if (filesets != null && !filesets.isEmpty()) {
      // explicit filesets given instead of model dir
      for (FileSet fileSet : filesets) {
        errors += processFileSet(fileSet);
      }
    }
    else {
      // all from model dir
      String[] files = modelDir.isDirectory() ? modelDir.list() : null;
      if (files != null && files.length > 0) {
        final FileSet fs = new FileSet();
        fs.setDirectory(modelDir.getPath());
        errors += processFileSet(fs);
      }
      else {
        getLog().warn((modelDir.exists() ? "empty modelDir " : "no modelDir ") + modelDir.getAbsolutePath());
      }
    }

    if (errors > 0) {
      throw new MojoFailureException(errors + " model errors");
    }

    try {
      getLog().info(Model.getInstance().getAllEntities().size() + " entities loaded");
    }
    catch (ModelException e) {
      throw new MojoExecutionException("cannot determine entities", e);
    }
  }


  /**
   * Processes a fileset to load the model.
   *
   * @param fileSet the fileset containing the model
   * @return the number of errors
   * @throws MojoExecutionException if model defaults or entity aliases could not be determined
   */
  protected int processFileSet(FileSet fileSet) throws MojoExecutionException {
    int errors = 0;

    if (fileSet.getDirectory() == null) {
      // directory missing: use modelDir as default
      fileSet.setDirectory(modelDir.getAbsolutePath());
    }

    File dir = new File(fileSet.getDirectory());
    String modelDirName = dir.getPath();

    try {
      Model model = Model.getInstance();
      model.setModelDefaults(getModelDefaults());
      model.setEntityAliases(getEntityAliases());
      Model.getInstance().loadFromDirectory(modelDirName, true);
    }
    catch (ModelException mex) {
      getLog().error("parsing model failed in directory " + modelDirName + ":\n" + mex.getMessage());
      errors++;
    }

    return errors;
  }


  @Override
  protected boolean validate() throws MojoExecutionException {
    if (super.validate()) {
      if (templateDir.exists()) {
        if (!templateDir.isDirectory()) {
          throw new MojoExecutionException(templateDir.getPath() + " is not a directory");
        }
      }
      else {
        templateDir.mkdirs();
        getLog().info("template directory created: " + templateDir.getPath());
        installTemplates();
      }
      return true;
    }
    return false;
  }


  /**
   * Copies the templates to the template directory.<br>
   * Overwrites any existing templates.
   *
   * @throws MojoExecutionException if failed
   */
  protected void installTemplates() throws MojoExecutionException {
    installTemplate(Constants.CATEGORY_PDO, "DomainImplementation.ftl");
    installTemplate(Constants.CATEGORY_PDO, "DomainInterface.ftl");
    installTemplate(Constants.CATEGORY_PDO, "PdoInterface.ftl");
    installTemplate(Constants.CATEGORY_PDO, "PersistenceImplementation.ftl");
    installTemplate(Constants.CATEGORY_PDO, "PersistenceInterface.ftl");
    installTemplate(Constants.CATEGORY_PDO, "RemoteInterface.ftl");
    installTemplate(Constants.CATEGORY_PDO, "RemoteImplementation.ftl");

    installTemplate(Constants.CATEGORY_OPERATION, "DomainImplementation.ftl");
    installTemplate(Constants.CATEGORY_OPERATION, "DomainInterface.ftl");
    installTemplate(Constants.CATEGORY_OPERATION, "OperationInterface.ftl");
    installTemplate(Constants.CATEGORY_OPERATION, "PersistenceImplementation.ftl");
    installTemplate(Constants.CATEGORY_OPERATION, "PersistenceInterface.ftl");
    installTemplate(Constants.CATEGORY_OPERATION, "RemoteInterface.ftl");
    installTemplate(Constants.CATEGORY_OPERATION, "RemoteImplementation.ftl");
  }

  private void installTemplate(String category, String template) throws MojoExecutionException {
    File dir = new File(templateDir, category);
    dir.mkdirs();
    File file = new File(dir, template);
    String path = "/templates/" + category + "/" + template;
    String text = loadResourceFileIntoString(path);
    try (PrintStream ps = new PrintStream(new FileOutputStream(file))) {
      ps.print(text);
      getLog().info("installed " + category + "-template " + template);
    }
    catch (IOException e) {
      throw new MojoExecutionException("cannot install template " + path, e);
    }
  }


  /**
   * Registers a handler for uncaught exceptions.
   */
  protected void registerUncaughtExceptionHandler() {
    Thread.setDefaultUncaughtExceptionHandler(getUncaughtExceptionHandler());             // all threads
    Thread.currentThread().setUncaughtExceptionHandler(getUncaughtExceptionHandler());    // overwrite existing handler for fx thread
  }

  protected void configureFx() {
    if (darkMode != null) {
      FxUtilities.getInstance().setDarkMode(darkMode);
    }
    else {
      FxUtilities.getInstance().setDarkMode(Platform.getPreferences().getColorScheme() == ColorScheme.DARK);
      Platform.getPreferences().colorSchemeProperty().subscribe(colorScheme -> {
        FxUtilities.getInstance().setDarkMode(colorScheme == ColorScheme.DARK);
        FxUtilities.getInstance().reapplyStyleSheets();
      });
    }
  }

  /**
   * Gets the stage, if this an interactive FX application.
   *
   * @return the stage, null if console application
   */
  protected abstract Stage getStage();

  private Thread.UncaughtExceptionHandler getUncaughtExceptionHandler() {
    return (t, e) -> {
      getLog().error("unhandled exception detected", e);
      Stage stage = getStage();
      if (stage != null) {
        Platform.runLater(() -> Fx.error(stage, e.getMessage(), e));
      }
    };
  }

}
