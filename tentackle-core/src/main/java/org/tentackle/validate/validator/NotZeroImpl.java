/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.validate.validator;

import org.tentackle.misc.ObjectUtilities;
import org.tentackle.validate.MandatoryBindingEvaluator;
import org.tentackle.validate.ValidationContext;
import org.tentackle.validate.ValidationResult;
import org.tentackle.validate.ValidationScope;
import org.tentackle.validate.ValidationSeverity;

import java.lang.annotation.Annotation;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

/**
 * Implementation of the {@link NotZero}-validator.
 *
 * @author harald
 */
public final class NotZeroImpl extends AbstractValidator<NotZero> implements MandatoryBindingEvaluator {

  private NotZero annotation;

  /**
   * Creates a {@link NotZero}-validator.
   */
  public NotZeroImpl() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public void setAnnotation(Annotation annotation) {
    this.annotation = (NotZero) annotation;
  }

  @Override
  public NotZero getAnnotation() {
    return annotation;
  }

  @Override
  public String getValue() {
    return annotation.value();
  }

  @Override
  public String getMessage() {
    return annotation.message();
  }

  @Override
  public String getErrorCode() {
    return annotation.error();
  }

  @Override
  public Class<? extends ValidationSeverity> getSeverity() {
    return annotation.severity();
  }

  @Override
  public int getPriority() {
    return annotation.priority();
  }

  @Override
  public Class<? extends ValidationScope>[] getScope() {
    return annotation.scope();
  }

  @Override
  public String getCondition() {
    return annotation.condition();
  }

  /**
   * {@inheritDoc}
   * <p>
   * If the annotation has a condition set, the mandatory property is dynamic.
   */
  @Override
  public boolean isMandatoryDynamic() {
    return !annotation.condition().isEmpty();
  }

  @Override
  public boolean isMandatory(ValidationContext validationContext) {
    return isConditionValid(validationContext) && !validate(validationContext).isEmpty();
  }

  @Override
  public List<? extends ValidationResult> validate(ValidationContext validationContext) {
    Object value = getValue().isEmpty() ?
                   // use object's value
                   validationContext.getObject() :
                   // replace with compound value which should be a script
                   getValue(validationContext);

    if (value == null) {
      return annotation.ignoreNulls() ? Collections.emptyList() :
             createValidationResultList("value is null", validationContext);
    }

    if (value instanceof Number) {
      return ObjectUtilities.getInstance().isZero((Number) value) ?
             createValidationResultList(MessageFormat.format(ValidatorBundle.getString("{0} is zero"), validationContext), validationContext) :
             Collections.emptyList();
    }
    else  {
      return createValidationResultList("value is not a number", validationContext);
    }
  }

}
