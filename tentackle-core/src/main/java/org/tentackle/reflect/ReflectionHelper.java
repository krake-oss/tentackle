/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.reflect;

import org.tentackle.common.StringHelper;
import org.tentackle.common.TentackleRuntimeException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Methods related to the reflection API.
 *
 * @author harald
 */
public final class ReflectionHelper {

  /**
   * Gets the basename of a classname(-like) string.
   * The basename is the last name of a pathname with dots.
   *
   * @param str the classname
   * @return the basename
   */
  public static String getClassBaseName(String str)  {
    return StringHelper.lastAfter(str, '.');
  }


  /**
   * Returns the absolute path name of the package of a class.<br>
   * Example:
   * <pre>
   *   com.example.myapp.Blah -&gt; "/com/example/myapp"
   * </pre>
   *
   * @param clazz the class
   * @return the package path name, "/" if class is in the root package
   */
  public static String getPackagePathName(Class<?> clazz) {
    String pkgName = clazz.getPackageName();
    return "/" + pkgName.replace('.', '/');
  }

  /**
   * Gets the package name of a classname(-like) string.<br>
   * Works also for inner classes.
   *
   * @param str the classname
   * @return the package name, "" if no package, str if str already is a package name
   */
  public static String getPackageName(String str) {
    int ndx;
    if (str.isEmpty() || Character.isUpperCase(str.charAt(0))) {
      return "";
    }
    while ((ndx = str.lastIndexOf('.')) >= 0 && ndx < str.length() - 1 && Character.isUpperCase(str.charAt(ndx + 1))) {
      str = str.substring(0, ndx);
    }
    return str;
  }

  /**
   * Get outer class name.
   *
   * @param name the probably inner classname
   * @return the outermost classname
   */
  public static String getOutermostClassName(String name) {
    String pkgName = ReflectionHelper.getPackageName(name);
    if (pkgName.length() < name.length() - 1) {
      String remainder = name.substring(pkgName.length() + (pkgName.isEmpty() ? 0 : 1));
      if (Character.isUpperCase(remainder.charAt(0))) {
        int ndx = remainder.indexOf('.');
        if (ndx >= 0) {
          remainder = remainder.substring(0, ndx);  // cut inner classes
        }
      }
      int ndx = remainder.indexOf('$');
      if (ndx >= 0) {
        remainder = remainder.substring(0, ndx);    // cut lambdas and anonymous classes
      }
      name = pkgName + (pkgName.isEmpty() ? "" : ".") + remainder;
    }
    return name;
  }

  /**
   * Creates a string from a method.<br>
   * This variant produces a much shorter string than {@link Method#toString()} and
   * shortens logging output.
   *
   * @param method the method
   * @return the string
   */
  public static String methodToString(Method method) {
    try {
      StringBuilder sb = new StringBuilder();
      sb.append(method.getDeclaringClass().getSimpleName()).append('.');
      sb.append(method.getName()).append('(');
      Class<?>[] params = method.getParameterTypes();
      for (int j = 0; j < params.length; j++) {
        sb.append(params[j].getSimpleName());
        if (j < (params.length - 1)) {
          sb.append(',');
        }
      }
      sb.append(')');
      return sb.toString();
    }
    catch (RuntimeException e) {
      return "<" + e + ">";
    }
  }

  /**
   * create a source-usable string from clazz.getName().
   * e.g. "[[[B" will be transformed to "byte[][][]"
   *
   * @param classname the classname
   * @return the converted string
   */
  public static String makeDeclareString(String classname) {
    if (classname.startsWith("["))  {
      // array
      StringBuilder buf = new StringBuilder();
      int len = classname.length();
      int i = 0;
      while (i < len) {
        if (classname.charAt(i) == '[') {
          buf.append("[]");
          ++i;
        }
        else  {
          break;
        }
      }

      String type = classname.substring(i);
      if (type.endsWith(";")) {
        // object
        type = type.substring(0, len - 1);
      }
      else if ("Z".equals(type)) {
        type = "boolean";
      }
      else if ("B".equals(type))  {
        type = "byte";
      }
      else if ("C".equals(type))  {
        type = "char";
      }
      else if ("D".equals(type))  {
        type = "double";
      }
      else if ("F".equals(type))  {
        type = "float";
      }
      else if ("I".equals(type))  {
        type = "int";
      }
      else if ("J".equals(type))  {
        type = "long";
      }
      else if ("S".equals(type))  {
        type = "short";
      }

      buf.insert(0, type);

      return buf.toString();
    }

    return classname;
  }



  private static final Map<Class<?>,Class<?>>  PRIMITIVES = new HashMap<>();

  static {
    PRIMITIVES.put(Boolean.TYPE, Boolean.class);
    PRIMITIVES.put(Byte.TYPE, Byte.class);
    PRIMITIVES.put(Character.TYPE, Character.class);
    PRIMITIVES.put(Short.TYPE, Short.class);
    PRIMITIVES.put(Integer.TYPE, Integer.class);
    PRIMITIVES.put(Long.TYPE, Long.class);
    PRIMITIVES.put(Double.TYPE, Double.class);
    PRIMITIVES.put(Float.TYPE, Float.class);
  }


  /**
   * Converts a primitive class to its wrapper class.
   *
   * @param primitiveClass the primitive
   * @return the wrapper
   * @throws ClassCastException if not a primitive class
   */
  public static Class<?> primitiveToWrapperClass(Class<?> primitiveClass) {
    Class<?> wrapperClass = PRIMITIVES.get(primitiveClass);
    if (wrapperClass == null) {
      throw new ClassCastException("not a primitive: " + primitiveClass);
    }
    return wrapperClass;
  }


  private static final Map<Class<?>,Class<?>> WRAPPERS = new HashMap<>();

  static {
    WRAPPERS.put(Boolean.class, Boolean.TYPE);
    WRAPPERS.put(Byte.class, Byte.TYPE);
    WRAPPERS.put(Character.class, Character.TYPE);
    WRAPPERS.put(Short.class, Short.TYPE);
    WRAPPERS.put(Integer.class, Integer.TYPE);
    WRAPPERS.put(Long.class, Long.TYPE);
    WRAPPERS.put(Double.class, Double.TYPE);
    WRAPPERS.put(Float.class, Float.TYPE);
  }

  /**
   * Converts a wrapper class to its primitive class.
   * @param wrapperClass the wrapper
   * @return the primitive
   * @throws ClassCastException if not a wrapper class
   */
  public static Class<?> wrapperToPrimitiveClass(Class<?> wrapperClass) {
    Class<?> primitiveClass = WRAPPERS.get(wrapperClass);
    if (primitiveClass == null) {
      throw new ClassCastException("not a wrapper: " + wrapperClass);
    }
    return primitiveClass;
  }


  /**
   * Tests if a given class implements or extends a set of interfaces or classes.
   *
   * @param superClasses the interfaces or classes
   * @param clazz the class to test
   * @param all true if all must match (logically and), false if at least one (logically or)
   * @return true if match
   */
  public static boolean isAssignableFrom(Class<?> clazz, Class<?>[] superClasses, boolean all) {
    if (clazz != null && superClasses != null) {
      for (Class<?> superClass: superClasses) {
        if (superClass.isAssignableFrom(clazz)) {
          if (!all) {
            return true;
          }
        }
        else  {
          if (all) {
            return false;
          }
        }
      }
      return all;
    }
    return true;
  }


  /**
   * Tests if given class has a set of annotations.
   * <p>
   * Notice: if the clazz or the annotations is null, true will be returned.
   *
   * @param clazz the class to test
   * @param annotations the annotations
   * @param all true if all must match (logically and), false if at least one (logically or)
   * @return true if match
   */
  public static boolean isAnnotationPresent(Class<?> clazz, Class<Annotation>[] annotations, boolean all) {
    if (clazz != null && annotations != null) {
      for (Class<? extends Annotation> annotation: annotations) {
        if (clazz.isAnnotationPresent(annotation)) {
          if (!all) {
            return true;
          }
        }
        else  {
          if (all) {
            return false;
          }
        }
      }
      return all;
    }
    return true;
  }


  /**
   * Extracts the inner class from a generic type.<br>
   * The type must be a parameterized generic type (usually determined by the binding)
   * for a single type argument.<br>
   * Works for wildcard types as well.<br>
   * Examples:
   * <pre>
   *   &#64;Bindable List&lt;OrgUnit&gt;
   *   &#64;Bindable List&lt;OrgUnit&lt;?&gt;&gt;
   *   &#64;Bindable List&lt;? extends OrgUnit&gt;
   *   &#64;Bindable List&lt;? extends OrgUnit&lt;?&gt;&gt;
   * </pre>
   * Will return <code>OrgUnit.class</code>.
   *
   * @param type the type
   * @param <T> the inner type
   * @return the class, null if type is not describing a class or is not parameterized
   */
  @SuppressWarnings("unchecked")
  public static <T> Class<T> extractGenericInnerTypeClass(Type type) {
    Class<T> clazz = null;
    if (type instanceof ParameterizedType parameterizedType) {
      Type[] typeArguments = parameterizedType.getActualTypeArguments();
      if (typeArguments.length == 1) {
        Type typeArgument = typeArguments[0];
        if (typeArgument instanceof Class) {
          clazz = (Class<T>) typeArgument;
        }
        else if (typeArgument instanceof WildcardType wildcardType) {
          typeArguments = wildcardType.getLowerBounds();
          if (typeArguments.length != 1) {
            typeArguments = wildcardType.getUpperBounds();
          }
          if (typeArguments.length == 1) {
            typeArgument = typeArguments[0];
            if (typeArgument instanceof Class) {
              clazz = (Class<T>) typeArgument;
            }
            else if (typeArgument instanceof ParameterizedType parTypeArg) {
              Type rawType = parTypeArg.getRawType();
              if (rawType instanceof Class) {
                clazz = (Class<T>) rawType;
              }
            }
          }
        }
        else if (typeArgument instanceof ParameterizedType parTypeArg) {
          Type rawType = parTypeArg.getRawType();
          if (rawType instanceof Class) {
            clazz = (Class<T>) rawType;     // something like OrgUnit<?>
          }
        }
      }
    }
    return clazz;
  }


  /**
   * Gets all declared fields of given class and its superclasses.
   *
   * @param clazz the class to get all declared and inherited fields
   * @param boundingClasses bounding classes that all classes must implement or extend, null if all (avoid!)
   * @param boundToAll true if all boundingClasses must match (logically and), false if at least one (logically or)
   * @param annotations the annotations, null if none
   * @param annotatedByAll true if all annotations must match (logically and), false if at least one (logically or)
   * @return all fields
   */
  public static Field[] getAllFields(Class<?> clazz,
                                     Class<?>[] boundingClasses, boolean boundToAll,
                                     Class<Annotation>[] annotations, boolean annotatedByAll) {
    Field[] allFields = new Field[0];
    while (clazz != null &&
           isAssignableFrom(clazz, boundingClasses, boundToAll) &&
           isAnnotationPresent(clazz, annotations, annotatedByAll)) {
      Field[] fields = clazz.getDeclaredFields();
      Field[] tempFields = new Field[allFields.length + fields.length];
      System.arraycopy(allFields, 0, tempFields, 0, allFields.length);
      System.arraycopy(fields, 0, tempFields, allFields.length, fields.length);
      allFields = tempFields;
      clazz = clazz.getSuperclass();
    }
    return allFields;
  }



  /**
   * Gets all declared methods of given class and its superclasses.
   *
   * @param clazz the class to get all declared and inherited methods
   * @param boundingClasses bounding classes that all classes must implement or extend, null if all (avoid!)
   * @param boundToAll true if all boundingClasses must match (logically and), false if at least one (logically or)
   * @param annotations the class annotations, null if none
   * @param annotatedByAll true if all annotations must match (logically and), false if at least one (logically or)
   * @return all methods
   */
  public static Method[] getAllMethods(Class<?> clazz,
                                       Class<?>[] boundingClasses, boolean boundToAll,
                                       Class<Annotation>[] annotations, boolean annotatedByAll) {
    Method[] allMethods = new Method[0];
    while (clazz != null &&
           isAssignableFrom(clazz, boundingClasses, boundToAll) &&
           isAnnotationPresent(clazz, annotations, annotatedByAll)) {
      Method[] methods = clazz.isInterface() ? clazz.getMethods() : clazz.getDeclaredMethods();
      Method[] tempMethods = new Method[allMethods.length + methods.length];
      System.arraycopy(allMethods, 0, tempMethods, 0, allMethods.length);
      System.arraycopy(methods, 0, tempMethods, allMethods.length, methods.length);
      allMethods = tempMethods;
      clazz = clazz.getSuperclass();
    }
    return allMethods;
  }



  /**
   * Checks whether a method is a getter.<br>
   * All non-void non-static methods without parameters are considered as getters.
   *
   * @param method the method
   * @return true if method is a getter
   */
  public static boolean isGetter(Method method) {
    return method.getReturnType() != Void.TYPE &&
           !Modifier.isStatic(method.getModifiers()) &&
           method.getParameterTypes().length == 0;
  }


  /**
   * Checks whether a method is a setter.<br>
   * All void non-static methods with a single parameter are considered as setters.
   * @param method the method
   * @return true if method is a setter
   */
  public static boolean isSetter(Method method) {
    return method.getReturnType() == Void.TYPE &&
           !Modifier.isStatic(method.getModifiers()) &&
           method.getParameterTypes().length == 1;
  }


  /**
   * Checks whether a method is a function that accepts one argument and produces a result.
   *
   * @param method the method
   * @return true if method is a function
   */
  public static boolean isFunction(Method method) {
    return method.getReturnType() != Void.TYPE &&
           !Modifier.isStatic(method.getModifiers()) &&
           method.getParameterTypes().length == 1;
  }


  /**
   * Skips the prefix of a method name.<br>
   * The first letter after the prefix must be in uppercase.<br>
   * Used to skip well-known prefixes like "set", "get", "is", etc...
   * <p>
   * Example:
   * <pre>
   *   skipMethodNamePrefix("getBlah", "get") -&gt; "blah"
   * </pre>
   *
   * @param methodName the method name
   * @param prefix the prefix to skip
   * @return the method name without the prefix, null if method does not start with prefix or letter after
   * prefix is not in uppercase
   */
  public static String skipMethodNamePrefix(String methodName, String prefix) {
    int prefixLength = prefix.length();
    if (methodName.length() > prefixLength && methodName.startsWith(prefix) &&
        Character.isUpperCase(methodName.charAt(prefixLength))) {
      return StringHelper.firstToLower(methodName.substring(prefixLength));
    }
    return null;
  }


  /**
   * Gets the method name reduced to the part used in paths if method is an attribute getter.
   * <p>
   * Examples:<br>
   * <pre>
   * getBlah -&gt; blah
   * isValid -&gt; valid (if boolean)
   * computeThisAndThat -&gt; computeThisAndThat
   * gettingWorse -&gt; gettingWorse
   * isselDussel -&gt; isselDussel
   * </pre>
   *
   * @param method the method
   * @return the method's reduced name, null if this is not an attribute getter
   */
  public static String getGetterPathName(Method method) {
    if (isGetter(method)) {
      String methodName = method.getName();
      String elementName = skipMethodNamePrefix(methodName, "get");
      if (elementName == null) {
        elementName = skipMethodNamePrefix(methodName, "is");
        if (elementName == null) {
          elementName = methodName;
        }
      }
      return elementName;
    }
    return null;
  }


  /**
   * Gets the method name reduced to the part used in paths if method is an attribute setter.
   * <p>
   * Examples:<br>
   * <pre>
   * setBlah -&gt; blah
   * runIt -&gt; runIt
   * </pre>
   *
   * @param method the method
   * @return the method's reduced name, null if this is not an attribute getter
   */
  public static String getSetterPathName(Method method) {
    if (isSetter(method)) {
      String methodName = method.getName();
      String elementName = skipMethodNamePrefix(methodName, "set");
      if (elementName == null) {
        elementName = methodName;
      }
      return elementName;
    }
    return null;
  }


  /**
   * Gets the method name reduced to the part used in binding or validation paths, if method is a "from"- or "with"-method.
   * <p>
   * Examples:<br>
   * <pre>
   * fromBlah -&gt; blah
   * withBlah -&gt; blah
   * </pre>
   *
   * @param method the method
   * @return the method's reduced name, null if this is not a from- or with-method
   */
  public static String getFromPathName(Method method) {
    if (isFunction(method) && !method.getReturnType().isPrimitive()) {    // must return an (immutable) object
      String methodName = method.getName();
      String elementName = skipMethodNamePrefix(methodName, "from");
      if (elementName == null) {
        elementName = skipMethodNamePrefix(methodName, "with");
        if (elementName == null) {
          elementName = methodName;
        }
      }
      return elementName;
    }
    return null;
  }


  /**
   * Gets the value of an attribute along a member path.
   * <p>
   * The attribute is accessible by either a getter-method
   * or a field, which must be public.
   * <p>
   * Example:<br>
   * <pre>
   * "invoice.invoiceLine.index"
   * </pre>
   * may correspond to the java path:
   * <pre>
   * this.getInvoice().getInvoiceLine().getIndex().
   * </pre>
   * <p>
   *
   * If a reference in the path is null, the returned value will be null by default.
   * This behaviour can be changed by appending an asterisk, i.e.
   * <code>"invoice.invoiceLine.index*"</code> will throw a TentackleRuntimeException
   * if invoice or invoiceLine is null.
   *
   * <p>
   * Notice: if path is null or empty the root object will be returned.
   * <p>
   * Throws TentackleRuntimeException if value cannot be retrieved
   *
   * @param root the root object the path refers to
   * @param path the member path in dot-notation relative to given object
   * @return the evaluated object
   *
   */
  public static Object getValueByPath(Object root, String path) {

    StringBuilder donePath = new StringBuilder();

    if (path != null) {

      // last char asterisk: no null references allowed
      boolean noNullRefs = path.lastIndexOf('*') == path.length() - 1;

      if (noNullRefs) {
        if (path.length() > 1) {
          path = path.substring(0, path.length() - 1);
        }
        else  {
          return root;
        }
      }

      Object value;
      StringTokenizer stok = new StringTokenizer(path, ".");

      while (stok.hasMoreTokens()) {

        if (root == null) {
          if (noNullRefs) {
            throw new TentackleRuntimeException("reference '" + donePath + "' is null");
          }
          else  {
            return null;
          }
        }

        Class<?> clazz = root.getClass();
        String element = stok.nextToken();
        if (element.endsWith("()")) {
          // cut trailing () as this is a common error if a method is meant
          element = element.substring(0, element.length() - 2);
        }
        if (!donePath.isEmpty()) {
          donePath.append('.');
        }
        donePath.append(element);

        Method method = getMethodForElement(clazz, element);
        if (method != null && isGetter(method)) {
          try {
            try {
              value = method.invoke(root);
            }
            catch (IllegalAccessException ex) {
              method.setAccessible(true);
              value = method.invoke(root);
            }
          }
          catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new TentackleRuntimeException("cannot retrieve '" + donePath + "' in " +
                                                root.getClass().getName() + " by method " + method.getName(), ex);
          }
        }
        else  {
          Field field = getFieldForElement(clazz, element);
          if (field != null) {
            try {
              try {
                value = field.get(root);
              }
              catch (IllegalAccessException ex) {
                field.setAccessible(true);
                value = field.get(root);
              }
            }
            catch (IllegalAccessException | IllegalArgumentException ex) {
              throw new TentackleRuntimeException("cannot retrieve '" + donePath + "' in " +
                                                  root.getClass().getName() + " by field " + field.getName(), ex);
            }
          }
          else  {
            throw new TentackleRuntimeException("cannot find '" + element + "' in " + clazz.getName());
          }
        }

        if (stok.hasMoreTokens()) {
          if (value == null && noNullRefs) {
            throw new TentackleRuntimeException("path '" + donePath + "' is null");
          }
          else  {
            root = value;
          }
        }
        else  {
          return value;
        }
      }
    }
    else  {
      return root;
    }

    throw new TentackleRuntimeException("no such path '" + donePath + "'");
  }


  /**
   * Gets the static value of an attribute along a member path starting
   * at a class.
   * <p>
   * The attribute is accessible by either a getter-method
   * or a field, which must be public.
   * <p>
   * Example:<br>
   * <pre>
   * "org.tentackle.misc.StringHelper.now"
   * </pre>
   * may correspond to the java path:
   * <pre>
   * SqlHelper.now()
   * </pre>
   *
   * @param clazz the class where the path starts
   * @param path the member path in dot-notation relative to given class
   * @return the object
   */
  public static Object getStaticValueByPath(Class<?> clazz, String path) {

    if (path != null) {

      String element = path;
      int ndx = path.indexOf('.');
      if (ndx > 0) {
        // only the first path member is static!
        element = path.substring(0, ndx);
        path = path.substring(ndx + 1);
      }
      else  {
        path = null;
      }

      Method method = getMethodForElement(clazz, element);
      if (method != null && Modifier.isStatic(method.getModifiers())) {
        try {
          try {
            return getValueByPath(method.invoke(null), path);
          }
          catch (IllegalAccessException ex) {
            method.setAccessible(true);
            return getValueByPath(method.invoke(null), path);
          }
        }
        catch (IllegalAccessException | InvocationTargetException | RuntimeException ex) {
          throw new TentackleRuntimeException("cannot retrieve '" + path + "' in " +
                                              clazz.getName() + " by method " + method.getName(), ex);
        }
      }
    }

    throw new TentackleRuntimeException("path is null");
  }


  /**
   * Finds the declared interface for a given class.<br>
   * Checks the interfaces of the class and its superclasses
   * until it finds a declared interface that is of the given interface type
   * or a subtype of.
   *
   * @param clazz the class
   * @param superIface the declared interface
   * @param <I> the type of the declared interface
   * @return the declared interface, null if not found
   */
  @SuppressWarnings("unchecked")
  public static <I> Class<? extends I> findInterface(Class<?> clazz, Class<I> superIface) {
    while (clazz != null) {
      Class<?>[] ifaces = clazz.getInterfaces();
      for (Class<?> iface : ifaces) {
        if (superIface.isAssignableFrom(iface)) {
          return (Class<? extends I>) iface;
        }
      }
      clazz = clazz.getSuperclass();
    }
    return null;
  }


  private static Method getMethodForElement(Class<?> clazz, String element) {
    Method method = null;
    String camelName = StringHelper.firstToUpper(element);   // camelcase name part
    try {
      method = clazz.getMethod(element);
    }
    catch (NoSuchMethodException ex1) {
      // try "is"
      try {
        method = clazz.getMethod("is" +  camelName);
      }
      catch (NoSuchMethodException ex2) {
        // try "get"
        try {
          method = clazz.getMethod("get" +  camelName);
        }
        catch (NoSuchMethodException ex3) {
          // return null below
        }
      }
    }
    return method;
  }

  private static Field getFieldForElement(Class<?> clazz, String element) {
    Field field = null;
    try {
      field = clazz.getField(element);
    }
    catch (NoSuchFieldException ex1) {
      // return null below
    }
    return field;
  }


  /**
   * prevent instantiation.
   */
  private ReflectionHelper() {}

}
