/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.io;

import java.io.IOException;
import java.io.Serial;
import java.net.Socket;

/**
 * ZIP-compressed client socket factory.
 *
 * @author harald
 */
public class CompressedClientSocketFactory extends ClientSocketFactory {

  @Serial
  private static final long serialVersionUID = 1L;

  /**
   * Creates the client socket factory with compression.
   */
  public CompressedClientSocketFactory() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public Socket createSocket(String host, int port) throws IOException {
    Socket socket = new CompressedSocket(host, port);
    if (socketConfigurator != null) {
      socketConfigurator.configure(socket);
    }
    return socket;
  }

}
