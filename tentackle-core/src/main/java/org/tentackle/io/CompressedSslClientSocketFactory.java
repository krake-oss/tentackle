/*
 * Tentackle - https://tentackle.org
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package org.tentackle.io;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import java.io.IOException;
import java.io.Serial;
import java.net.Socket;
import java.util.Objects;

/**
 * ZIP-compressed client socket factory
 *
 * @author harald
 */
public class CompressedSslClientSocketFactory extends SslRMIClientSocketFactory implements SocketConfiguratorHolder {

  @Serial
  private static final long serialVersionUID = 1L;

  private volatile SocketConfigurator socketConfigurator;   // the optional socket configurator


  /**
   * Creates a compressed SSL client socket factory.
   */
  public CompressedSslClientSocketFactory() {
    // see -Xlint:missing-explicit-ctor since Java 16
  }

  @Override
  public Socket createSocket(String host, int port) throws IOException {
    Socket socket = new CompressedSocketWrapper(super.createSocket(host, port));
    SocketConfigurator localConfigurator = socketConfigurator;
    if (localConfigurator != null) {
      localConfigurator.configure(socket);
    }
    return socket;
  }


  @Override
  public int hashCode() {
    int hash = 5;
    hash = 11 * hash + getClass().hashCode();
    hash = 11 * hash + Objects.hashCode(socketConfigurator);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final CompressedSslClientSocketFactory other = (CompressedSslClientSocketFactory) obj;
    return SocketConfigurator.equals(this.socketConfigurator, other.socketConfigurator);
  }

  @Override
  public SocketConfigurator getSocketConfigurator() {
    return socketConfigurator;
  }

  @Override
  public void setSocketConfigurator(SocketConfigurator socketConfigurator) {
    this.socketConfigurator = socketConfigurator;
  }

}
